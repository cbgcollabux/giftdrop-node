var express = require('express');
var request = require('request');
var router = express.Router();

var token = 'eyJhbGciOiJSUzI1NiIsImtpZCI6ImVjZWFkZGUwMGMzN2U1OWJhNDU0NzdlMjg1MDA3OWQzNDhhMTE0M2IifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZ2Z0bm93IiwibmFtZSI6IkZvcmQiLCJwaWN0dXJlIjoiaHR0cDovL2xvY2FsaG9zdC9hc3NldHMvdXBsb2FkL3BocC9maWxlcy8xMTJkOWNiN2EyNzRiOWFmY2ZiMi5wbmciLCJhdWQiOiJnZnRub3ciLCJhdXRoX3RpbWUiOjE1MDg3MzgwOTYsInVzZXJfaWQiOiJUaVRQVUxnbUxrTXNRblZjZHhoU0cwaWtQbDAyIiwic3ViIjoiVGlUUFVMZ21Ma01zUW5WY2R4aFNHMGlrUGwwMiIsImlhdCI6MTUwODczODA5NywiZXhwIjoxNTA4NzQxNjk3LCJlbWFpbCI6IjExMkBmb3JkLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyIxMTJAZm9yZC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.oy4aMwZSkgMWGWEin0iRNVL3ej6w2nbp9b2qZyhaZwJn0Wnq6UTzR-PEaPTeTrexGEMFIOg1P-Fc-Ek0tHZpoBz38BFZ6l2WZ_b7wTIR__eOyr7GdDEf1Y3AOuwCc_yCFpdopDjbiNkaxXeZfqDkPvxdoxFzccJmGLXIPainX0Sqpo7jPjf2619tejcU4bP9dCtqVSFtLYp94X9Xxc8UJHil0ktKikrrNhEzZC3gFlQaTaUvJS3XxOydj29qgojooyM7w7Os_zGsofHZyCUxz_MLmTA2CrJJpIi4xdGDjQIMANzRYTovTGBxpLr5ditLXi8Wv8xINd7onvqTc9vLUQ';

var options = {
  url: 'https://gftnow.appspot.com/_ah/api/businessService/v1/listCompanyCategories',
  headers: {
    'X-GFT-AUTH-TOKEN': token,
    'X-GFT-AUTH-PROVIDER': 'firebase',
    'Content-type': 'application/json'
  },
  method: "POST"
};

function callback(error, response, body) {
	console.log(error);
	console.log(response);
	console.log(body);
  if (!error && response.statusCode == 200) {
    var info = JSON.parse(body);
    console.log(info.stargazers_count + " Stars");
    console.log(info.forks_count + " Forks");
  }
}

request(options, callback);

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('test2 with a resource');
});

module.exports = router;
