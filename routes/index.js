var express = require('express');
var router = express.Router();
var giftdrop = require('giftdrop');
var request = require('request');
var upload = require('express-fileupload');
var date = require('node-datetime');
var md5 = require('md5');
var ip = require('ip');
var stripe = require('stripe')('sk_test_EIHOlymZUWjdMnz8W0Wdrvxx');
var geocoder = require('reverse-geocoding');
var publicIp = require('public-ip');

var ipaddress = '';
publicIp.v4().then(ip => {
  ipaddress = ip;
});

//test clmm
router.use(upload());

function shuffle(string) {
  var parts = string.split('');
  for (var i = parts.length; i > 0;) {
      var random = parseInt(Math.random() * i);
      var temp = parts[--i];
      parts[i] = parts[random];
      parts[random] = temp;
  }
  return parts.join('');
}

router.post('/restricted-login', function(req, res, next){
  if(req.body.password == "1qa@WS3edd"){
    req.session.restricted = false;
    res.send({result:"success", session:req.session,body:req.body});
  } else {
    res.send({result:"error"});
  }
});

router.get('/getAddress', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  res.send(ipaddress);
});

//get restricted page
router.get('/', function(req, res, next) {
  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    req.session.restricted = true;
    res.render('restricted');
  }else{
    res.render('business');
  }
});

//get home page
router.get('/home', function(req, res, next) {
  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }
  res.render('index');
});


//get business page.
router.get('/business', function(req, res, next) {
  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }
  res.render('business');
});

//post business/login
router.post('/business/login', function(req, res, next){
  var options = giftdrop.sendRequest(req.body.token, null, 'login');

  request(options, function(error, response, body){
    req.session.userInfo = JSON.parse(body).userInfo;
    req.session.token = req.body.token;
    res.send(JSON.parse(body));
  });
});

//post image upload
router.post('/upload', function(req, res, next){

  if(req.files){
    var file = req.files.filename;
    var hashed = shuffle(md5(date.create().format('H:M:S')));
    var filename = hashed.substr(0,20) + ".png";
    file.mv(__dirname+"/../public/uploads/"+filename, function(err){
      if(err) res.send({"response":"error!", "error":err});
    });

    /*var uploadPath = "http://130.211.222.114:9090/uploads/"+filename;
    res.send({"response":"upload success!", "file":filename, "url":uploadPath});*/

    res.send({"response":"upload success!", "file":filename, "url":"/uploads/"+filename});
  }else{
    res.send({"response":"error"});
  }
});

//post image crop
router.post('/crop', function(req, res, next){
  if(req.files){
    var file = req.files.filename;
    var hashed = shuffle(md5(date.create().format('H:M:S')));
    var filename = hashed.substr(0,20) + ".png";
    file.mv(__dirname+"/../public/uploads/cropped/"+filename, function(err){
      if(err) res.send({"response":"error!", "error":err});
    });
  
    var uploadPath = "http://130.211.222.114:9090/uploads/cropped/"+filename;

    res.send({"response":"crop success!", "file":filename, "url":uploadPath});
  }
});

// get onboarding page
router.get('/onboarding', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  res.render('onboarding');
});

//post onboarding submit
router.post('/onboarding/submit', function(req, res, next){
  //console.log(req.body);
  req.session.token = req.body.token;
  //req.session.company = req.body.company;
  req.session.name = req.body.name;
  req.session.email = req.body.email;
  req.session.password = req.body.password;
  req.session.logoURL = req.body.logoURL;

  var options = giftdrop.sendRequest(req.body.token, null, 'createBusiness');

  request(options, function(error, response, body){
    //console.log(JSON.parse(body));
    req.session.userInfo = JSON.parse(body)
    res.send(JSON.parse(body));
  });

  //res.send({"token":req.body.token, "company":req.body.company});
});

//get onboarding-1 page
router.get('/onboarding-1', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var token = req.session.token;

  if(token){
    /*var options = giftdrop.sendRequest(token, null, 'listCompanyCategories');
    request(options, function(error, response, body){
      console.log("----------------------------------------------------------------");
      console.log(JSON.parse(body).productCategories.length);
      res.render('onboarding-1', { categories: JSON.parse(body).productCategories });
    });*/
    res.render('onboarding-1');
  } else {
    res.redirect('/onboarding');
  }
});

//post onboarding-1 submit
router.post('/onboarding-1/submit', function(req, res, next) {
  req.session.businesscategory = req.body.category;
  res.send({"response":"success!", "category":req.body.category});
});

//get onboarding-2 page
router.get('/onboarding-2', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var businesscategory = req.session.businesscategory;
  if(businesscategory){
    res.render('onboarding-2');
  } else {
    res.redirect('/onboarding-1');
  }
});

//post onboarding-2 submit
router.post('/onboarding-2/submit', function(req, res, next){
  var token = req.session.token;

  if(token){

    req.session.firstname = req.body.firstname;
    req.session.lastname = req.body.lastname;
    req.session.website = req.body.website;
    req.session.phone = req.body.phone;
    req.session.address = req.body.address;
    req.session.city = req.body.city;
    req.session.zip = req.body.zip;
    req.session.state = req.body.state;
    req.session.about = req.body.about;

    var account = {};
    account.state = 2;
    account.notes = req.body.phone;
    account.logoURL = req.session.logoURL;
    account.website = req.body.website;
    account.about = req.body.about;
    account.title = req.body.phone;

    var raw = {};
    raw.state = 2;
    raw.notes = req.body.phone;
    raw.name = req.session.name;
    raw.account = account;
    raw.address = req.body.address+", "+req.body.city+", "+req.body.state;
    raw.category = req.session.businesscategory;


    req.session.userInfo.firstName = req.session.firstname;
    req.session.userInfo.lastName = req.session.lastname;

    var body1 = {};
    body1.userInfo = {};
    body1.userInfo.identifier = req.session.userInfo.identifier;
    body1.userInfo.firstName = req.session.userInfo.firstName;
    body1.userInfo.lastName = req.session.userInfo.lastName;
    
    var options1 = giftdrop.sendRequest(token, body1, 'changeUserInfo');

    request(options1, function(error, response, body1){
         
        var options = giftdrop.sendRequest(token, raw, 'createMyCompany');
    
        request(options, function(error, response, body){

          res.send({"response":"success!" });  
        
        });
         
    });

    


  } else {
    res.redirect('/onboarding-2');
  }
});

//get onboarding-3 page
router.get('/onboarding-3', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var token = req.session.token;

  if(token){
    req.session = null;
    res.render('onboarding-3');
  } else {
    res.redirect('/business');
  }
});

//get logout
router.get('/logout', function(req, res, next){

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  req.session.destroy();
  res.redirect('/business');

});

//get profile page.
router.get('/profile', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var session = req.session;

  //res.send(session.name);

  if(session.userInfo){
    var options = giftdrop.sendRequest(session.token, null, 'findMyCompanyProfile');
    request(options, function(error, response, body){
      var b = JSON.parse(body);
      session.company = b;
      res.render('profile', {email: session.userInfo.contactRepository.contacts[0].contact, identifier:b.identifier, name:b.name, acct_identifier:b.account.identifier, acct_created:b.account.created, notes:b.account.notes, logoURL:b.account.logoURL, acct_state:b.account.state, title:b.account.title, about:b.account.about, website:b.account.website, created:b.created, activated:b.activated, phone:b.notes, state:b.state, address:b.address, category:b.category});
      /*request(giftdrop.sendRequest(session.token, null, 'listCompanyCategories'), function(error, response, body){
        var categories = JSON.parse(body);
        res.render('profile', {email: session.userInfo.contactRepository.contacts[0].contact, identifier:b.identifier, name:b.name, acct_identifier:b.account.identifier, acct_created:b.account.created, notes:b.account.notes, logoURL:b.account.logoURL, acct_state:b.account.state, title:b.account.title, about:b.account.about, website:b.account.website, created:b.created, activated:b.activated, phone:b.notes, state:b.state, address:b.address, category:b.category, pc:JSON.stringify(categories)});
      });*/
    });
  } else {
    res.redirect('/business');
  }
});

//post profile changeMyCompany
router.post('/profile/changeMyCompany', function(req, res, next){
  var token = req.session.token;
  var options = giftdrop.sendRequest(token, JSON.parse(req.body.company), 'changeMyCompany');
  request(options, function(error, response, body){
    res.send({"response":"success!", "response":response, "body":body});
  });
});

//get dashboard page
router.get('/dashboard', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var session = req.session;

  var now = new Date(); 
  now.setDate(now.getDate() + 1);
  var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
  
  //res.send(now_utc);
  var dateRange = {from:'2016-11-03T01:10:44.670Z', to:now_utc};

  var options = giftdrop.sendRequest(session.token, dateRange, 'collectMyCompanyStatistics');
  request(options, function(error, response, body){
    var stats = JSON.parse(body);
    console.log(stats);
    if(session.userInfo){
      res.render('dashboard', {stats:stats});
    } else {
      res.redirect('/business');
    }
  });
});

//get campaigns page
router.get('/campaigns', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var session = req.session;
  var dateRange = {from:'2016-11-03T01:10:44.670Z', to:'2018-11-03T01:10:44.670Z'};

  var options = giftdrop.sendRequest(session.token, dateRange, 'listMyCompanyProducts');
  request(options, function(error, response, body){
    if(session.userInfo){
      res.render('campaigns', {listProducts:JSON.parse(body)});
    }
    else
      res.redirect('/business');
  });
});

//get campaign_details page.
router.get('/campaign_details', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var session = req.session;

  var id = {productIdentifier:req.query.id};

  var options = giftdrop.sendRequest(session.token, id, 'findProduct');
  request(options, function(error, response, body){
    console.log(JSON.parse(body));
    
    var b = JSON.parse(body);
    var latlng_arr = new Array();

    b.productAndDrops.drops.forEach(function(drop){
      latlng_arr.push(drop.location.latitude+","+drop.location.longitude);
    });

    function unique(array){
        return array.filter(function(el, index, arr) {
            return index == arr.indexOf(el);
        });
    }

    var latlng_arr_unique = unique(latlng_arr);

    console.log(latlng_arr_unique);
    
    if(session.userInfo)
      res.render('campaign_details', {logoURL:session.userInfo.account.logoURL, product:b.productAndDrops.product, drops:b.productAndDrops.drops, latlng:latlng_arr_unique});
    else
      res.redirect('/business');
  });
});

//get new_campaign page
router.get('/new_campaign', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var session = req.session;
  var campaign = {};

  
  if(session.campaign)
  {
    campaign = session.campaign;
  }else{
    campaign.name = "";
  }

 //res.send(session);

  if(session.userInfo)
    res.render('new_campaign',{campaign: campaign});
  else
    res.redirect('/business');
});

//post new_campaign submit
router.post('/new_campaign/submit', function(req, res, next){
  var campaign = {};
  campaign.name = req.body.campaignname;
  campaign.type = req.body.type;
  campaign.description = req.body.description;
  campaign.terms = req.body.terms;
  campaign.gftvendor = req.body.gftvendor;
  campaign.gftamount = req.body.gftamount;
  campaign.image = req.body.image;

  req.session.campaign = campaign;
  res.send({"response":"success!"});
});

//post listGiftCards
router.post('/listGiftCards', function(req, res, next) {
  var body = {};
  var options = giftdrop.sendRequest(req.session.token, body, 'listGiftCards');

   request(options, function(error, response, body){
    if(body)
      res.send({"listGiftcards1":JSON.parse(body)});
    else
      res.send('error');
  });
});

//get new_campaign-1 page
router.get('/new_campaign-1', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var session = req.session;

 
  if(session.userInfo){
    if(session.campaign){
      
      var drops = "";

      if(session.drops){
       for(var i =0;i<session.drops.length;i++){
          drops.push(session.drops[i].lat+";"+session.drops[i].lng);
        }
      }

      res.render('new_campaign-1', {drops_str:drops,drops:(session.drops)?session.drops:{}, campaign: (session.campaign)?session.campaign:{} });
    }else{
      res.redirect('/new_campaign');
    }
  } else {
    res.redirect('/business');
  }
});

//post new_campaign-1 submit
router.post('/new_campaign-1/submit', function(req, res, next){
  var drops = {};
  drops.barcodes = req.body.barcodes;
  drops.drops = JSON.parse(req.body.drops);
  drops.barcodetype = req.body.barcodetype;
  drops.offer = req.body.offer;
  drops.date1 = req.body.date1;
  drops.time1 = req.body.time1;
  drops.redemption_details = req.body.redemption_details;
  req.session.drops = drops;
  res.redirect('/new_campaign-2');
});

//get new_campaign-2 page
router.get('/new_campaign-2', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var session = req.session;
  
  
  if(session.userInfo){
     if(typeof session.drops !== "undefined")
      res.render('new_campaign-2', {session:session});
      else
     res.redirect('/new_campaign-1');
   } else {
    res.redirect('/business');
  }
});

//post new_campaign-2 getSessionDrops
router.post('/new_campaign-2/getSessionDrops', function(req, res, next){
  var session = req.session;
  res.send({"result":"success", "drops":session.drops.drops});
});

//get campaign_paypal page
router.get('/campaign_paypal', function(req, res, next) {

  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var session = req.session;

  if(!session.userInfo){
    res.redirect("/business");
    return;
  }
  var total_qty = 0;

  for(var i=0; i < session.drops.drops.length; i++){
    total_qty += Number(session.drops.drops[i].quantity);
    console.log(total_qty);
  }

  var charge = Number( session.drops.offer ) * total_qty;
  var total_charge;

  if(session.campaign.gftvendor == -1)
    total_charge = charge * 0.03;
  else
    total_charge = charge * 1.03;   


  var notes = {};
  notes.company = session.company.name;
  var product = {};
  product.visibility = "Visible";
  product.creator = "";
  product.state = "Draft";
  product.notes = JSON.stringify(notes);
  product.name = session.campaign.name;
  product.message = session.campaign.description;
  product.photoURL = session.campaign.image;
  product.termsAndConditions = session.campaign.terms;
  product.vendor = (session.campaign.gftvendor==-1)?null:session.campaign.gftvendor;
  product.amount = Number( session.drops.offer );
  product.expiration = "2027-04-15T23:00:26.669Z";
  product.howToRedeem = session.drops.redemption_details;

  var barcodes = session.drops.barcodes.split(",");

  var locations = new Array();
  
  for(var i=0; i<session.drops.drops.length; i++){
    for(var j=0; j<Number(session.drops.drops[i].quantity); j++){
      var location = {};
      location.latitude = session.drops.drops[i].lat;
      location.longitude = session.drops.drops[i].lng;
      locations.push(location);
    }
  }

  var newDrops = new Array();
  for(var i=0; i<total_qty; i++){
    var drop = {};
    drop.barcode = barcodes[i];
    drop.senderVisibility = "Visible";
    drop.location = locations[i];
    newDrops.push(drop);
  }

  var productAndDrops = {};
  productAndDrops.product = product;
  productAndDrops.drops = newDrops;
  
  if(session.draft_created==-1)
  {
      var p_id = session.productIdentifier
    
      var campaign_summary = {session:session, total_qty:total_qty, total_charge:total_charge.toFixed(2), p_id:p_id};
      res.render('campaign_paypal', campaign_summary);

  }else{
    
    request(giftdrop.sendRequest(session.token, {productAndDrops:productAndDrops}, 'createDraftProduct'), function(error, response, body){
        
        var productAndDrops = JSON.parse(body);
        var p_id = productAndDrops.productAndDrops.product.identifier;
        req.session.draft_created = true;
        req.session.productIdentifier = p_id;

        var charge = Number( session.drops.offer ) * total_qty;
        var total_charge;


        if(session.campaign.gftvendor == -1)
          total_charge = charge * 0.03;
        else
          total_charge = charge * 1.03;
   
        var campaign_summary = {session:session, total_qty:total_qty, total_charge:total_charge.toFixed(2), p_id:p_id};
        res.render('campaign_paypal', campaign_summary);

    });
    
  }
});

//post campaign_paypal stripe
router.post('/campaign_paypal/stripe', function(req, res, next) {
  var session = req.session;
  if(session.userInfo){

    var total_qty = 0;

  for(var i=0; i < session.drops.drops.length; i++){
    total_qty += Number(session.drops.drops[i].quantity);
    console.log(total_qty);
  }

  var charge = Number( session.drops.offer ) * total_qty;
  var total_charge;

  if(session.campaign.gftvendor == -1)
    total_charge = charge * 0.03;
  else
    total_charge = charge * 1.03;   


    var charge1 = stripe.charges.create({
      amount: total_charge,
      currency: 'usd',
      description: 'Drops Payment',
      source: req.body.token,
    });

    res.send({"result":"success", "charge":JSON.stringify(charge)});
  } else {
    res.send('session expired');
  }
});

//post campaign_paypal buyDraftProduct
router.get('/campaign_paypal/buyDraftProduct', function(req, res, next) {
  
  if (typeof(req.session.restricted) == "undefined" || req.session.restricted == true ){
    res.render('restricted');
  }

  var session = req.session;

  if(session.userInfo){

    var body = {};
    body.productIdentifier = session.productIdentifier;
    body.paymentConfirmation = {};
    body.paymentConfirmation.nonce = 'tok_visa';
    body.paymentConfirmation.paymentProviderName = 'Stripe';
    
    //res.send(body);
    var options = giftdrop.sendRequest(req.session.token, body, 'buyDraftProduct');
    
    request(options, function(error, response, body){

      delete req.session.draft_created;
      delete req.session.campaign;
      delete req.session.drops;
      res.redirect('/campaigns');
     
    });
  
  } else {
    res.redirect('/business');
  }

});

module.exports = router;