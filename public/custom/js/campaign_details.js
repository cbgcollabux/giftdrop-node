$(document).ready(function() {
	var map;

	function getAddress(latlng_str, callback){
		var latlngStr = latlng_str.split(",",2);
	    var latlng = new google.maps.LatLng(parseFloat(latlngStr[0]), parseFloat(latlngStr[1]));
	    geocoder = new google.maps.Geocoder();
	    geocoder.geocode({'latLng': latlng}, function(results, status){
	        if(status === google.maps.GeocoderStatus.OK){
	            if(results[0]){
	                callback(results[0].formatted_address);
	            }
	        }
	    });
	}

	var ctr = 0;
	$(".geocode").each(function(){
		var latlng = $(this).data("latlng");
		getAddress(latlng, function(address){
			ctr++;
			console.log(ctr);
			console.log(address);
			$("#1geocode"+ctr).html(address);
			$("#2geocode"+ctr).html(address);
		});
	});

	/*reverseGeocode();

	function reverseGeocode(){
		var latlng_arr = new Array();

		$(".geocode").each(function(){
			var latlng = {};
			latlng.lat = $(this).data("lat");
			latlng.lng = $(this).data("lng");
			latlng_arr.push(latlng);
		});

		console.log(latlng_arr);

		jQuery.ajax({
			url:"campaign_details/reverse_geocode",
			type: "post",
			dataType: "json",
			data: { latlng:latlng_arr },
			success: function(data){
				console.log(data);
			},error: function(err){
				console.log(err.responseText);
			}
		});
	}*/

	$("#add-drop").click(function(){
		$("#tmp-add-drop").css("display","none");
		$("#tmp-with-panel").css("display","initial");
	});

	google.maps.event.addDomListener(window, 'load', init);
	
	$(".drop-rounded-header").click(function(){
		$(this).parent().find(".drop-rounded-body").show();
	});

	$(".drop-rounded-body a").click(function(){
	 	var text = $(this).text();
	 	$(this).parent().parent().find(".drop-rounded-header span").text(text);
	 	$(this).parent().parent().parent().find(".drop-rounded-header span").text(text);
		$(this).parent().parent().hide();
	});

	$("#select-view").change(function(){
		if($(this).val() == "List"){
			$("#view-map").hide();
			$("#view-list").show();
		}
		if($(this).val() == "Map"){
			$("#view-list").hide();
			$("#view-map").show();
		}
	});

	if($("#api_error").length>0){
		$("#gftnow-alert-modal-content").html($("#api_error").val());
		$("#gftnow-alert-modal").css({ "position":"fixed","left":0,"right":0,margin: "0 auto" }).modal("show");
	}

	$("#logout").show();
	$("#business-menu-campaign").addClass("active");

	$(".selectpicker_").selectpicker();
	$("#nextButton").click(function(){
		if($("#requiredInput").text("")){
			$("#requiredField").show();
		}
	});

	$(".drops").each(function(){
		if($(this).find("i").hasClass("text-success")){
			$(this).find("i").attr({
				rel: "popover-hover",
				'data-trigger': "hover",
				'data-placement': "top",
				'data-content': "Drop was redeemed. Click The code to see details."
			})
		}
	});

	$("[rel=popover-hover]").popover({trigger: "hover"});

	$(".flavor-campaigns").click(function(){
		$(".campaign-drops-desc", this).slideToggle("fast");
		$(".campaign-drops-desc", this).bind("click", function(e){
			e.stopPropagation();
		});
	});

	$(".drops").click(function(){
		var drop = $(".drop", this).attr("data-barcode");

		JsBarcode("#barcode", drop, {format: "CODE128", width: 1});

		if($(this).find("i").hasClass("text-success")){
			$("#drop-info-checked").fadeIn("fast");
		}
		if($(this).find("i").hasClass("text-default")){
			$("#drop-info-unchecked").fadeIn("fast");
		}
	});
	
	$("#close-drop-info-checked").click(function(){
		$("#drop-info-checked").fadeOut("fast");
	});

	$("#close-drop-info-unchecked").click(function(){
		$("#drop-info-unchecked").fadeOut("fast");
	});
});

function DropControl(controlDiv, map) {
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginTop = '10px';
    controlUI.style.textAlign = 'center';
    controlDiv.appendChild(controlUI);


    var controlText = document.createElement('div');
    controlText.style.color = '#514F4F';
    controlText.style.fontFamily = 'GothamRndRegular';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '15px';
    controlText.style.paddingRight = '15px';
    controlText.innerHTML = 'Drop Pin &nbsp;&nbsp; <img src="/img/green_pin.png">';
    controlUI.appendChild(controlText);

    controlUI.addEventListener('click', function() {
      
    });
}

function codeAddress(dropAddress, dropQty){	
	console.log(dropAddress);
	console.log(dropQty);
    var address = $("#gftnow-infobox").text();
   
    geocoder.geocode( { 'address': dropAddress}, function(results, status){
        if (status == google.maps.GeocoderStatus.OK)
        {
        	var overlay = new CustomMarker(
				results[0].geometry.location,
				map, {}, dropQty
			);

			var infowindow = new google.maps.InfoWindow({
	        	content: document.getElementById("gftnow-infobox")
	        });

    		google.maps.event.addListener(infowindow, 'domready', function() {
				var iwOuter = $('.gm-style-iw');
				var iwBackground = iwOuter.prev();
				iwBackground.children(':nth-child(2)').css({'display' : 'none'});
				iwBackground.children(':nth-child(4)').css({'display' : 'none'});
				iwBackground.children(':nth-child(1)').css({'display' : 'none'});
				iwBackground.children(':nth-child(3)').css({'display' : 'none'});
			});

			map.setCenter(results[0].geometry.location);
        }
        else{
        	console.log("Geocode was not successful for the following reason: " + status);
        }
    });
}

function init() {
	geocoder = new google.maps.Geocoder();

    var mapOptions = {
        zoom: 11,

        center: new google.maps.LatLng(40.6700, -73.9400), // New York
        disableDefaultUI: true,
        mapTypeControl: true,
        streetViewControl: true,
        mapTypeControlOptions: {
        	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },

        styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
    };

    var mapElement = document.getElementById('map');

    map = new google.maps.Map(mapElement, mapOptions);

    $(".address").each(function(){
    	codeAddress($(this).val(), parseInt($(this).attr("data-qty")));
    });

}