$(document).ready(function(){
	$("body").css("max-height", "900px");
	var placed_drops = JSON.parse( $("#placed_drops").val() );
	var spendings = JSON.parse( $("#spendings").val() );

	console.log(placed_drops);
	console.log(spendings);

	$("#business-menu-dashboard").addClass("active");

	$("#drop-rounded-header-1").unbind("click").bind("click", function(){
		$("#drop-rounded-body-1").slideToggle("fast");
	});

	$("#drop-rounded-header-2").unbind("click").bind("click", function(){
		$("#drop-rounded-body-2").slideToggle("fast");
	});

	if ( placed_drops.length > 0 ){
		var lmorris = Morris.Line({
			element: "placed-drops-graph",
			xkey : "period",
			ykeys : ["placed_drops"],
			labels : ["Placed Drops"],
			data : placed_drops,
			pointSize : 5,
			hideHover : "auto",
			lineColors: ["#fdd05e","#8bc24a","#75C5F9", "#d8222a"],
			parseTime: false,
		});

		$(window).on("resize", function(){
			lmorris.redraw();
	    });
	}else{
		$("#placed-drops-graph").html("<center><h2>No Data to display</h2></center>");
	}


	if ( spendings.length > 0 ){
		var lmorris2 = Morris.Line({
			element: "spendings-graph",
			xkey : "period",
			ykeys : ["spendings"],
			labels : ["Spendings"],
			data : spendings,
			pointSize : 5,
			hideHover : "auto",
			lineColors: ["#8bc24a","#75C5F9", "#d8222a"],
			parseTime: false,
		});

		$(window).on("resize", function(){
			lmorris2.redraw();
		});
	}else{
		$("#spendings-graph").html("<center><h2>No Data to display</h2></center>");
	}
});