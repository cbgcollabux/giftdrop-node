$(document).ready(function() {

	$(".gftnow-checkbox").click(function(){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
		} else {
			$(this).addClass("active");
		}
		checkvalidation();
	});

    $("#zip").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#zip").on("keyup", function(){
    	jQuery(document).ajaxStart(function(){
    		$("#mask2").hide();
    		$("#state-invalid").hide();
			$("#state").hide();
			$("#state-loader").show();
		});

		jQuery(document).ajaxComplete(function(){
			$("#mask2").hide();
			$("#state-loader").hide();
			$("#state").show();
		});

		$.ajax({
			url: "https://zip.getziptastic.com/v2/US/" + $(this).val(),
			cache: false,
			dataType: "json",
			type: "GET",
			success: function(result, success) {
				console.log("success");
				$("#state").val(result.state);
				$("#state-invalid").hide();
			},
			error: function() {
				console.log("error");
				$("#state").val();
				$("#state-invalid").show();
			}
		});
    });

	checkvalidation();

	jQuery.validator.addMethod("website", function(value, element) {
	  return this.optional(element) || /^(https?:\/\/)?([\w\d\-_]+\.)+\/?/.test(value);
	}, "Please provide a valid website.");
	
	jQuery.validator.addMethod("phone", function(value, element) {
	  return this.optional(element) || /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(value);
	}, "Please provide a valid phone number.");

	jQuery.validator.addMethod("zipcode", function(value, element) {
  		return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
	}, "Please provide a valid zipcode.");

	$("input,textarea").keyup(function(){
		checkvalidation();
	});

	$("input,textarea").focus(function(){
		checkvalidation();
	});

	 $("#form1").validate(
	 {
	 	submitHandler: function(form){
	 		//form.submit();
            var firstname = $("#firstname").val();
            var lastname = $("#lastname").val();
            var website = $("#website").val();
            var phone = $("#phone").val();
            var address = $("#address").val();
            var city = $("#city").val();
            var zip = $("#zip").val();
            var state = $("#state").val();
            var about = $("#about").val();

            jQuery.ajax({
                type: 'post',
                url: 'onboarding-2/submit',
                dataType: 'json',
                data: { firstname:firstname, lastname:lastname, website:website, phone:phone, address:address, city:city, zip:zip, state:state, about:about, },
                success: function(data){
                    console.log(data);
                    window.location = "onboarding-3";
                },
                error: function(err){
                    console.log(err.responseText);
                }
            });
	 	},
	 	rules: {
	 		firstname: {
	 			required: true
	 		},
	 		lastname: {
	 			required: true
	 		},
	 		website: {
	 			required: true,
	 			website: true
	 		},
	 		phone: {
	 			required: true,
	 			phone: true
	 		},
	 		address: {
	 			required: true
	 		},
	 		city: {
	 			required: true
	 		},
	 		zip: {
	 			required: true,
	 			zipcode: true
	 		},
	 		state: {
	 			required: true
	 		}
	 	}
	 });
});

function checkvalidation()
{
var err = 0;

$("input").each(function(){
	if ( $(this).hasClass("invalid") )
		err++;
});

$("textarea").each(function(){
	if ( $(this).hasClass("invalid") )
		err++;
});

if( $("input[name='firstname']").val() == "" )
	err++;

if( $("input[name='lastname']").val() == "" )
	err++;

if( $("input[name='website']").val() == "" )
	err++;

if( $("input[name='phone']").val() == "" )
	err++;

if( $("input[name='address']").val() == "" )
	err++;

if( $("input[name='city']").val() == "" )
	err++;

if( $("input[name='zip']").val() == "" )
	err++;

if( $("input[name='state']").val() == "" )
	err++;

if( !$(".gftnow-checkbox").hasClass("active") )
	err++;

if(err==0){
	$("#submit").removeClass("gftnow-btn-default");
	$("#submit").addClass("gftnow-btn-success");
	$("#submit").prop("disabled",false);
	$("#submit").css({color: "#fff"});
}else{
	$("#submit").addClass("gftnow-btn-default");
	$("#submit").removeClass("gftnow-btn-success");
	$("#submit").prop("disabled",true);
	$("#submit").css({color: "#000"});
}
}