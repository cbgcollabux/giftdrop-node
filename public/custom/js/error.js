$(document).ready(function() {
	$("#view-errors").click(function(){
		$("#errors-stack").slideToggle();
	});

	$("#prev-page").click(function(){
		window.history.back();
	});
});