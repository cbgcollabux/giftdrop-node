$(document).ready(function() {
    
    /*var imgArr =[ {"img":"parallax.jpg","by":"Cody Sumter"},{"img":"parallax1.jpg","by":"Trey Ratcliff"},{"img":"parallax2.jpg","by":"Bjorn Kleemann"},{"img":"parallax3.jpg","by":"Dave Morrow"},{"img":"parallax4.jpg","by":"Neal Grosskopt"},{"img":"parallax5.jpg","by":"Andreas Klose"},{"img":"parallax6.jpg","by":"+AnushElangonvan"},{"img":"parallax7.jpg","by":"Ray Bilcliff"}];
    imgArr = shuffle(imgArr);

    var initbg = imgArr[Math.floor(Math.random() * (imgArr.length)) ];
    $("#particles-js").css("background-image", "url(img/" + initbg.img + ")")
	$("#by").text(initbg.by);

    animateBackground();

	function animateBackground() {
	    window.setTimeout(function(){
	        var url = imgArr[imgArr.push(imgArr.shift()) - 1];
	        $('#particles-js').delay(30000).fadeOut(500, function(){
	            $(this).css("background-image", "url(img/" + url.img + ")")
	            $("#by").text(url.by);
	        }).fadeIn(500, animateBackground())
	    });
	}*/

	init();

	$("#submit.bttn-rounded").click(function(){
		$(this).closest('form').submit();
	});

	$("#mask2").css("z-index", "9999");

	$("#email").keypress(function(e){
		if(e.which == 13){
			$(this).blur();
			$('#submit').focus().click();
		}
	});

	$("#password").keypress(function(e){
		if(e.which == 13){
			$(this).blur();
			$('#submit').focus().click();
		}
	});

	$("#loginform").validate({
		errorPlacement: function(){
			return false;
		},
		rules:{
			email: "required",
			password: "required"
		},
		submitHandler: function(form){
			console.log("submit");

			console.log($(form.password).val());

			var password = $(form.password).val();

			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
			jQuery(document).ajaxComplete(function(){ $("#mask2").hide(); });
					
			jQuery.ajax({
				url: "restricted-login",
				type: "post",
				dataType : "json",
				data: { password:password },
				success: function(data){
					console.log(data);
					if(data.result == "success"){
						$("#mask2").show();
						window.location = '/home';
					} else {
						$("#errorCode .alert").html("Invalid Password!");
						$("#errorCode").show();
					}
				},error: function(err){
					console.log(err.responseText);
				}
			});
		}
	});
});

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
function init(){
	var width = $(window).width();
	console.log(width);
	var panelWidth = 0.5;

	if(width<=414)
		panelWidth = 0.95;
	else if(width<=320)
		panelWidth = 1;

	

	$('#slider-one').movingBoxes({
	startPanel   : 2,      // start with this panel
	reducedSize  : 0.5,    // non-current panel size: 80% of panel size
	wrap         : false,   // if true, the panel will "wrap" (it really rewinds/fast forwards) at the ends
	buildNav     : false,   // if true, navigation links will be added
	navFormatter : function(){ return "&#9679;"; } // function which returns the navigation text for each panel
	,panelWidth   : panelWidth,
	initAnimation: true,
	easing       : 'swing',
	completed: function(e, slider, tar){
		
		$("#movingbox-label").text(slider.curPanel+"/3");
	}
	});

	var mb = $('#slider-one').data('movingBoxes');

	$("#movingbox-next").bind("click", function(){
		mb.goForward();
	});

	$("#movingbox-prev").bind("click", function(){
		mb.goBack();
	});
}