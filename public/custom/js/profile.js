$(document).ready(function(){

	$(".category").each(function(){
		if($("#current-category").val() == $(this).data("value"))
			$(this).addClass("active");
	});

	//img upload add crop
	$("#fileupload").change(function(){
		$("#uploadForm").ajaxSubmit({
			url: 'upload',
			type: 'post',
			dataType: 'json',
			success: function(data) {
				console.log(data);
	            $("#mask2").hide();

	            $(".img-container").html("<img id='image2' src='"+data.url+"' alt='Picture'>");
        		
            	image = $('#image2');

            	var cropper = image.cropper({
				    aspectRatio: 1/1,
				    minContainerWidth: 555,
				    minContainerHeight: 300,
				    cropBoxResizable: false,
				    built: function () {
				      image.cropper("setCropBoxData", { width: 250, height: 250 });
				    },
				    crop: function(e) {
				      console.log(e.x);
				      console.log(e.y);
				      console.log(e.width);
				      console.log(e.height);
				      console.log(e.rotate);
				      console.log(e.scaleX);
				      console.log(e.scaleY);
				    }
				});
              
				$("#mask2").hide();
				$("#uploadModal").modal({backdrop: 'static', keyboard: false}, "toggle");
	        },
			error: function(err){
				console.log(err.responseText);
				$("#mask2").hide();
			}
		});
	});

	$("#cropnow").bind("click",function(){
		$("#mask2").show();
		image.cropper('getCroppedCanvas',{
			width:300,
			height:300,
			fillColor: '#fff',
			imageSmoothingEnabled: true,
			imageSmoothingQuality: 'high'
		}).toBlob(function (blob) {
      		$("#cropnow").unbind("click");
      		var reader = new FileReader();
      		reader.readAsDataURL(blob); 
			reader.onloadend = function() {
				function base64ToFile(base64Data, tempfilename, contentType) {
					contentType = contentType || '';
					var sliceSize = 1024;
					var byteCharacters = atob(base64Data);
					var bytesLength = byteCharacters.length;
					var slicesCount = Math.ceil(bytesLength / sliceSize);
					var byteArrays = new Array(slicesCount);

					for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
						var begin = sliceIndex * sliceSize;
						var end = Math.min(begin + sliceSize, bytesLength);

						var bytes = new Array(end - begin);
						for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
							bytes[i] = byteCharacters[offset].charCodeAt(0);
						}
							byteArrays[sliceIndex] = new Uint8Array(bytes);
						}
					var file = new File(byteArrays, tempfilename, { type: contentType });
					return file;
				}

			    var base64data = reader.result.substr(reader.result.indexOf(',') + 1);
			    var file = base64ToFile(base64data, 'tempName', 'image/png');

			    var fd = new FormData();
			    fd.append('filename', file);

			    jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });

			    jQuery.ajax({
			    	url: "crop",
					type: "post",
					dataType: "json",
					data: fd,
					processData: false,
					contentType: false,
					success: function(data) {
						console.log(data);
						$("#mask2").show();
						$("#logoURL").val(data.url);
						updatecompanyprofile();
					},
					error: function(err){
						console.log(err.responseText);
						$("#mask2").hide();
						$("#uploadModal").modal("toggle");
					}
			    });
			}
  		});
	});

	$(".selectpicker_").selectpicker();

	$("#business-menu-company-profile").addClass("active");

	$("#choose-category-chooseBtn").click(function(){
		var newcat = $("#choose-category-div").find(".uploadLogo.uploadLogo2.active").attr("data-value");
		$(".content-info[data-id=\'1\']").hide();
		$(".loader[data-id=\'1\']").fadeIn("fast");

		if(newcat)
			$("#category").val(newcat);

		$("#choose-category-div").fadeOut("fast");
		jQuery(document).ajaxStart(function(){ $("#mask2").hide(); });

		updatecompanyprofile();
	});

	$(".custom-circle").mouseenter(function(){
        $(".balloon-container1").show();
    });

    $(".custom-circle").mouseleave(function(){
        $(".balloon-container1").hide();
    });

	$("#notif-info").click(function(){
        $("#plans-balloon").toggle();
    });

	$("[data-toggle=\'popover\']").popover({html:true});

    $(".balloon-container1").mouseenter(function(){
        $(".balloon-container1").show();
    });

    $(".balloon-container1").mouseleave(function(){
        $(".balloon-container1").hide();
    });

	$(".uploadLogo2").click(function(){
		$(".uploadLogo2").removeClass("active");
		$(this).addClass("active");
		$("#businesscategory").val( $(this).attr('data-value') );
	});

	$("#choose-category").click(function(){
		$("#choose-category-div").fadeIn("fast");
	});
	
	$("#choose-category-backBtn").click(function(){
		$("#choose-category-div").fadeOut("fast");
	});

	function editClick(id, val){
		var value = $(".content-info[data-id=\'"+id+"\']").attr("data-value");
		$(".content-info[data-id=\'"+id+"\']").hide();
		$(".content-info-edit[data-id=\'"+id+"\']").val(val).fadeIn("fast");
		$(".save-btn[data-id=\'"+id+"\']").fadeIn("fast");
		$(".close-btn[data-id=\'"+id+"\']").fadeIn("fast");

		$(".save-btn[data-id=\'"+id+"\']").click(function(){
			var newValue = $(".content-info-edit[data-id=\'"+id+"\']").val();
			console.log("data-id: "+id+" value: "+newValue);

			$(".content-info-edit[data-id=\'"+id+"\']").hide();
			$(".save-btn[data-id=\'"+id+"\']").hide();
			$(".close-btn[data-id=\'"+id+"\']").hide();
			$(".loader[data-id=\'"+id+"\']").fadeIn("fast");

			if(id == 2)
				$("#phone").val(newValue);

			if(id == 3)
				$("#acct_website").val(newValue);

			if(id == 4)
				$("#address").val(newValue);

			if(id == 5)
				$("#acct_about").val(newValue);

			jQuery(document).ajaxStart(function(){ $("#mask2").hide(); });

			updatecompanyprofile();
		});

		$(".close-btn[data-id=\'"+id+"\']").click(function(){
			$(".content-info-edit[data-id=\'"+id+"\']").hide();
			$(".save-btn[data-id=\'"+id+"\']").hide();
			$(".close-btn[data-id=\'"+id+"\']").hide();
			$(".content-info[data-id=\'"+id+"\']").fadeIn("fast");
		});
	}

	$(".content-info").mouseenter(function() {
		var id = $(this).attr("data-id");
		var val = $(this).attr("data-val");
    	$(this).find("a[data-id=\'"+id+"\']").fadeIn("fast");
    	$(this).find("a[data-id=\'"+id+"\']").click(function(){
	    	if(id == 1){
	    		$("#choose-category-div").fadeIn("fast");
	    	} else {
		    	editClick(id, val);
			}
		});
  	}).mouseleave(function() {
		$(this).find("a").fadeOut("fast");
	});

	function updatecompanyprofile(){
		var account = {};
		account.identifier = $("#acct_identifier").val();
		account.created = $("#acct_created").val();
		account.notes = $("#notes").val();
		account.logoURL = $("#logoURL").val();
		account.state = $("#acct_state").val();
		account.title = $("#title").val();
		account.about = $("#about").val();
		account.website = $("#website").val();

		var company = {};
		company.identifier = $("#identifier").val();
		company.name = $("#name").val();
		company.account = account;
		company.created = $("#created").val();
		company.activated = $("#activated").val();
		company.notes = $("#phone").val();
		company.state = $("#state").val();
		company.address = $("#address").val();
		company.category = $("#category").val();
		
		console.log(company);

		jQuery.ajax({
			url:"profile/changeMyCompany",
			type: "post",
			dataType: "json",
			data: { company:JSON.stringify(company) },
			success: function(data){
				location.reload();
				console.log(data);
			},error: function(err){
				console.log(err.responseText);
			}
		});
	}
});