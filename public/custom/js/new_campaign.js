var basic;
var images = new Array();
var start = 0;
var init = 0;

$(document).ready(function(){
	$("#business-menu-campaign").addClass("active");
	

	checkvalidation();
	
	$("input, textarea").focus(function(){
		checkvalidation();
	});

	$("input, textarea").keyup(function(){
		checkvalidation();
	});

	$(".gftnow-checkbox").click(function(){
		$("#type").val( $(this).attr('data-value'));
	});

	if($("#campaignname").val != "" && $("#description").val() != "" && $("#terms").val() != ""){
		$("#campaignname").trigger("click");
		$("#description").trigger("click");
		$("#terms").trigger("click");
	}

	$(".gftnow-checkbox[data-name=\'type\']").click(function(){
		$(".gftnow-checkbox[data-name=\'type\']").removeClass("active");
		$(this).addClass("active");
		checkvalidation();
		console.log($(this).attr("data-value"));
		if($(this).attr("data-value")=="gift-card"){
			if ($("ul#uploader-content li").length > 1){
				$("ul#uploader-content li:first-child").remove();
			}
			$("#description").val('');
			$("#terms").val('');
			/*$("#custom-upload").find("#fileupload").remove();
*/
		
			$("#custom-upload").show();
			$("#custom-upload").unbind("click");

			$("#custom-upload").click(function(){
				return false;
			});
			
			$("#custom-upload").bind("click", function(){

				
				jQuery.ajax({
					url: "listGiftcards",
					type: "post",
					dataType: "json",
					data: { func: "listGiftcards", token: $("#token").val() },
					success: function(data){
						
						console.log(data.listGiftcards1.vendors);
						var otherbrand_content = "<h1 style=\'font-size:25px;\'>Gift Card <a href=\'javascript:void(0);\' id=\'close-other-brand\' class=\'pull-right\' style=\'margin-top:-18px;margin-right:-4px;color:#D3D2D2;\'><i class=\'fa fa-close\'></i></a></h1><div style=\'width:280px;\'><p style=\'font-family:GothamRndRegular;font-size:15px;\'>Please choose the type of gift card you would like to purchase</p><br/></div>";
						
						otherbrand_content += "<center><img class=\'gftnow-hide\' id=\'my-own\' style=\'cursor:pointer;\' src=\'img/gift-card-1.png\'/></center>"+
												"<hr/><div class=\'active-main-panel\' style=\'margin-top:0px;\'>"+
														"<div class=\'row active-list-title\'>"+
															"<div class=\'col-md-3\' align=\'center\'><strong>Preview</strong></div>"+
															"<div class=\'col-md-3\' align=\'center\'><strong>Name</strong></div>"+
															"<div class=\'col-md-3\' align=\'center\'><strong>Details</strong></div>"+
															"<div class=\'col-md-3\' align=\'center\'><strong>Amount</strong></div>"+
														"</div>"+
											   "</div>";

						otherbrand_content += "<div class=\'active-main-panel\' id=\'gftnow-scrollbar\' style=\'margin-top:0px;overflow-y:auto;max-height:300px;\'>";

						for(var i =0;i<data.listGiftcards1.vendors.length;i++)
						{
								console.log(data.listGiftcards1.vendors[i]);
								var values_str = data.listGiftcards1.vendors[i].validValues;
								var values = values_str.split(',');
								console.log(values);

								otherbrand_content+="<div class=\'row active-list other-brand\' data-status=\'"+data.listGiftcards1.vendors[i].catalogStatus+"\' data-terms=\'"+data.listGiftcards1.vendors[i].termsAndConditions+"\' data-name=\'"+data.listGiftcards1.vendors[i].name+"\' data-description=\'"+data.listGiftcards1.vendors[i].description+"\' data-amount=\'"+data.listGiftcards1.vendors[i].validValues+"\' data-id=\'"+data.listGiftcards1.vendors[i].identifier+"\' data-logo=\'"+data.listGiftcards1.vendors[i].logoURL+"\'>"+
									"<div class=\'col-md-3\'><img style=\'height:50px;width:85px;\' src=\'"+data.listGiftcards1.vendors[i].logoURL+"\'></div>"+
									"<div class=\'col-md-3\'>"+data.listGiftcards1.vendors[i].name+"</div>"+
									"<div class=\'col-md-3\'><a class=\'read-disclaimer\' href=\'javascript:void(0);\' data-toggle=\'collapse\' data-target=\'#desc"+data.listGiftcards1.vendors[i].identifier+"\'>Read Disclaimer</a></div>"+
									"<div class=\'col-md-3\'>$ "+( (values[0]) ? values[0]+" - "+values[values.length-1] : values[values.length-1] )+"</div>"+
									"<div id=\'desc"+data.listGiftcards1.vendors[i].identifier+"\' class=\'col-md-11 collapse\' align=\'left\' style=\'font-family:GothamRndLight;top:20px;bottom:20px;left:30px;\'>"+data.listGiftcards1.vendors[i].description+"<br/><br/></div>"+
								"</div>";
						}

						otherbrand_content+= "</div><br/><span id=\'otherbrand-error-msg\' style=\'font-family:GothamRndMedium;color:red;\'></span><br/>"+
											 "<br/><label id=\'choose-brand\' type=\'button\' class=\'btn btn-success gftnow-btn-rounded\'>&nbsp;&nbsp;CHOOSE&nbsp;&nbsp;</label>";


						$("#gft-card-div").fadeOut("fast");

						$("#other-brand-content").html(otherbrand_content);

						$("#other-brand").fadeIn("fast");
						$("#close-other-brand").bind("click", function(){
							$("#other-brand").fadeOut("fast");
						});

						var selected = 0;
						var ob = false;
						var yo = false;

						$(".other-brand").bind("click", function(){
							$(".other-brand").removeClass("active");
							$("#my-own").attr("src", "img/gift-card-1.png");
							$(this).addClass("active");
							if(selected == 0)
								selected++;
							ob = true;
							yo = false;
							console.log(ob);
							console.log(yo);
						});

						$("#my-own").bind("click", function(){
							$(this).attr("src", "img/gift-card-1.1.png");
							$(".other-brand").removeClass("active");
							if(selected == 0)
								selected++;
							yo = true;
							ob = false;
							console.log(ob);
							console.log(yo);										
						});

						$("#choose-brand").bind("click", function(){
							//$("#campaignname").trigger("click");
							$("#description").trigger("click");
							$("#terms").trigger("click");										
							if(selected == 0){
								$("#otherbrand-error-msg").html("Please select a type of gift card!");
							} else {
								if(yo == true){
									//$("#fileupload").trigger("click");
									$("#gftvendor").val( -1 );
									$("#gftamount").val( -1 );
									$("#custom-upload-content").html("<h4>Upload</h4><p>5mb max size<br/>543 x 303 recommended size</p>");
									$("#other-brand").fadeOut("fast");
								}
								if(ob == true){
									var logolocation = $(".other-brand.active").attr('data-logo');
									$("#gftvendor").val( $(".other-brand.active").attr('data-id') );
									$("#gftamount").val( $(".other-brand.active").attr('data-amount') );
									$("#gftstatus").val( $(".other-brand.active").attr('data-status') );
									$("#gftterms").val( $(".other-brand.active").attr('data-terms') );
									$("#gftname").val( $(".other-brand.active").attr('data-name') );
									$("#gftdescription").val( $(".other-brand.active").attr('data-description') );

									$("#description").val( $(".other-brand.active").attr('data-description').replace(/<\/?[^>]+(>|$)/g, "") );
									$("#terms").val( $(".other-brand.active").attr('data-terms').replace(/<\/?[^>]+(>|$)/g, "") );
									//images[2] = {location:"assets/upload/php/files/amazon-lg.png"};
									images[0] = {location:logolocation};
									$("#campaignimagetype").val("image");
									$("#campaignimage").val(images[0].location);
									start=0;
									
									renderScroll();
									$("#custom-upload").addClass("active").html("<img src=\'"+logolocation+"\' style=\'width:100%;height:auto;max-width:100%;max-height:100%;\'><input id=\'fileupload\' type=\'file\' name=\'files[]\'/\'>");
									$("#other-brand").fadeOut("fast");
								}
							}

							checkvalidation();
						});

						
					},error: function(err){
						console.log(err.responseText);
					}
				});
			});
			$("#custom-upload").html("<br/><br/><br/><br/><div class=\'label2\'><div id=\'custom-upload-content\'></div><h4>Choose Gift Card</h4></div></div>");
			$("#campaignimagetype").attr("value", "");
			$("#campaignimage").attr("value", "");
		}
		if($(this).attr("data-value")=="free-product"){
			if ($("ul#uploader-content li").length > 1){
				$("ul#uploader-content li:first-child").remove();
			}
			$("#description").val('');
			$("#terms").val('');
			
			$("#custom-upload").show().unbind("click").bind("click", function(){
					$("#fileupload").trigger("click");
			});


			$("#custom-upload").html("<br/><br/><br/><br/><div class=\'label2\'><div id=\'custom-upload-content\'></div><h4>Upload</h4><p>5mb max size<br/>543 x 303 recommended size</p></div></div>");
			$("#gftvendor").val( -1 );
			$("#gftamount").val( -1 );
			$("#campaignimagetype").attr("value", "");
			$("#campaignimage").attr("value", "");
		}
	});
	
	$(".gftnow-checkbox[data-name=\'type\']").click(function(){
		$(".gftnow-checkbox[data-name=\'type\']").removeClass("active");
		$(this).addClass("active");
		checkvalidation();
	});

	$(".gftnow-fader img").bind("click",function(){
		start++;
		renderScroll();
	});

	$(".drop-rounded-header").click(function(){
		$(this).parent().find(".drop-rounded-body").show();
	});

	$(".drop-rounded-body a").click(function(){
		var text = $(this).text();
		$(this).parent().parent().find(".drop-rounded-header span").text(text);
		$(this).parent().parent().parent().find(".drop-rounded-header span").text(text);
		$(this).parent().parent().hide();
	});

	if($("#gfttype").val()=="free-product"){
		$(".gftnow-checkbox[data-value='free-product']").click();

		setTimeout(function(){
			$("#description").val($("#description").attr('value'));
			$("#terms").val($("#terms").attr('value'));
			$("#custom-upload").html("<img src='"+$("#gftimage").val()+"' style='width:100%;height:auto;max-width:100%;max-height:100%;'/>");
			$("#custom-upload").addClass("active");
			checkvalidation();
		},2000);
	}else if($("#gfttype").val()=="gift-card"){
		$(".gftnow-checkbox[data-value='gift-card']").click();

		setTimeout(function(){
			$("#description").val($("#description").attr('value'));
			$("#terms").val($("#terms").attr('value'));
			$("#custom-upload").html("<img src='"+$("#gftimage").val()+"' style='width:100%;height:auto;max-width:100%;max-height:100%;'/>");
			$("#custom-upload").addClass("active");
			checkvalidation();
		},2000);

	}


	$("#fileupload").change(function(){


		var fileExt = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];

		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExt) == -1) {
			$("#gftnow-alert-modal").find(".modal-body").html("<center>Only image files are accepted</center>");
			$("#gftnow-alert-modal").modal("show");
    	} else {
			$("#uploadForm").ajaxSubmit({
				url: 'upload',
				type: 'post',
				dataType: 'json',
				success: function(data) {
					console.log(data);
		            $("#mask2").hide();

		            $(".img-container").html("<img id='image2' src='"+data.url+"' alt='Picture'>");
	        		
	            	image = $('#image2');

	            	var cropper = image.cropper({
					    aspectRatio: 2/1,
					    minContainerWidth: 555,
					    minContainerHeight: 300,
					    dragMode: "move",
					    built: function () {
					      image.cropper("setCropBoxData", { width: 350, height: 195 });
					    },
					    crop: function(e) {
					      console.log(e.x);
					      console.log(e.y);
					      console.log(e.width);
					      console.log(e.height);
					      console.log(e.rotate);
					      console.log(e.scaleX);
					      console.log(e.scaleY);
					    }
					});
	              
					$("#mask2").hide();
					$("#uploadModal").modal({backdrop: 'static', keyboard: false}, "toggle");
		        },
				error: function(err){
					console.log(err.responseText);
					$("#mask2").hide();
				}
			});
		}
	});

	$(".cropnow").bind("click",function(){
		$("#mask2").show();
		image.cropper('getCroppedCanvas',{
			width:350,
			height:195,
			fillColor: '#fff',
			imageSmoothingEnabled: true,
			imageSmoothingQuality: 'high'
		}).toBlob(function (blob) {
      		$("#cropnow").unbind("click");
      		var reader = new FileReader();
      		reader.readAsDataURL(blob); 
			reader.onloadend = function() {
				function base64ToFile(base64Data, tempfilename, contentType) {
					contentType = contentType || '';
					var sliceSize = 1024;
					var byteCharacters = atob(base64Data);
					var bytesLength = byteCharacters.length;
					var slicesCount = Math.ceil(bytesLength / sliceSize);
					var byteArrays = new Array(slicesCount);

					for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
						var begin = sliceIndex * sliceSize;
						var end = Math.min(begin + sliceSize, bytesLength);

						var bytes = new Array(end - begin);
						for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
							bytes[i] = byteCharacters[offset].charCodeAt(0);
						}
							byteArrays[sliceIndex] = new Uint8Array(bytes);
						}
					var file = new File(byteArrays, tempfilename, { type: contentType });
					return file;
				}

			    var base64data = reader.result.substr(reader.result.indexOf(',') + 1);
			    var file = base64ToFile(base64data, 'tempName', 'image/png');

			    var fd = new FormData();
			    fd.append('filename', file);

			    jQuery.ajax({
			    	url: "crop",
					type: "post",
					dataType: "json",
					data: fd,
					processData: false,
					contentType: false,
					success: function(data) {
						console.log(data);
						$("#campaignimage").val(data.url);
						//$("#custom-upload").addClass("active").html("<img src=\'/uploads/cropped/"+data.file+"\' style=\'width:100%;height:auto;max-width:100%;max-height:100%;\'><input id=\'fileupload\' type=\'file\' name=\'files[]\'/\'>");
						$("#custom-upload").addClass("active").html("<img src=\'/uploads/cropped/"+data.file+"\' style=\'width:100%;height:auto;max-width:100%;max-height:100%;\'>");
						
						checkvalidation();
						$("#mask2").hide();
						$("#uploadModal").modal("toggle");
					},
					error: function(err){
						console.log(err.responseText);
						$("#mask2").hide();
						$("#uploadModal").modal("toggle");
					}
			    });
			}
  		});
	});

	$("#form1").validate({
	 	errorPlacement: function(){
			return false;
		},
	 	submitHandler: function(form){
	 		var type = $(".gftnow-checkbox.active[data-name=\'type\']").attr("data-value");
	 		var image = $("#custom-upload.uploadLogo.uploadLogo2.active").find("img").attr("src");
	 		var err = 0;
	 		var message = "";
	 		if(!type){
	 			message+="Please select Gift type<br/>";
	 			err++;
	 		}

	 		if(!image){
	 			message+="Please select or upload image<br/>";
	 			err++;
	 		}

	 		if(err==0){
	 			//form.submit();
	 			var campaignname = $("#campaignname").val();
	 			var type = $(".gftnow-checkbox.active[data-name=\'type\']").attr("data-value");
	 			var description = $("#description").val();
	 			var terms = $("#terms").val();
	 			var gftvendor = $("#gftvendor").val();
	 			var gftamount = $("#gftamount").val();
	 			var image = $("#campaignimage").val();

	 			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
				jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });

	 			jQuery.ajax({
	 				url: "new_campaign/submit",
	 				type: "post",
	 				dataType: "json",
	 				data: {campaignname:campaignname, type:type, description:description, terms:terms, gftvendor:gftvendor, gftamount:gftamount, image:image},
	 				success: function(data){
	 					console.log(data);
	 					window.location.href = "new_campaign-1";
	 				},
	 				error: function(err){
	 					console.log(err.responseText);
	 				}
	 			});
	 		}else{
	 			$("#gftnow-alert-modal-content").html(message);
				$("#gftnow-alert-modal").modal("show");
	 		}
	 	},
	 	rules: {
	 		terms: "required",
	 		description: "required",
	 		campaignname: "required"
	 	}
	 });

	renderScroll();

});

function checkvalidation(){
	console.log("validate");
	var err = 0;

	if( $("input[name=\'campaignname\']").val() == "" )
		err++;

	if( $("textarea[name=\'description\']").val() == "" )
		err++;

	if( $("textarea[name=\'terms\']").val() == "" )
		err++;

    var type = $(".gftnow-checkbox.active[data-name=\'type\']").attr("data-value");
	var image = $("#custom-upload.uploadLogo.uploadLogo2.active").find("img").attr("src");

	if(!type)
		err++;

	if(!image)
		err++;
		
	console.log(type);
	console.log(image);

	if(err==0){
		$("#submit").removeClass("gftnow-btn-default");
		$("#submit").addClass("gftnow-btn-success");
	}else{
		$("#submit").addClass("gftnow-btn-default");
		$("#submit").removeClass("gftnow-btn-success");
	}
}

function renderScroll(){
	var html = "";

	if(init==0&&$("#campaignimage").val()!=""){
		$(".uploadLogo2").each(function(){
			$(this).removeClass("active");
		});

		images.push({location:$("#campaignimage").val(), type: $("#campaignimagetype").val() });
	}
	
	$('.gtfnow-uploader-slider ul li:not(:last)').remove();

	for(var i=images.length-1;i>=0;i--){
		if(images[i].isvideo){

			html = '<li class="image-selection card-preview-small">'+
					'<div class="card-preview-small uploadLogo uploadLogo2 active">'+

						'<video class="video"><source src="'+images[i].location+'"></video>'+
					'</div>'+
				'</li>';
			}else{
				html = '<li class="image-selection card-preview-small">'+
					'<div class="card-preview-small uploadLogo uploadLogo2 active">'+

						'<img src="'+images[i].location+'"/>'+
					'</div>'+
				'</li>';
			}
		
	
		$(".gtfnow-uploader-slider ul").prepend(html);
		$('.video').click(function(){this.paused?this.play():this.pause();});
	}


	init++;
	checkvalidation();
	

	$(".image-selection").bind("click",function(){
		$(".uploadLogo2").removeClass("active");
		$(this).find(".uploadLogo2").addClass("active");

		if($(this).find(".uploadLogo2 img").length>0){
			$("#campaignimage").val($(this).find(".uploadLogo2 img").attr("src"));
			$("#campaignimagetype").val("image");
		}
			
		if($(this).find(".uploadLogo2 video").length>0){
			$("#campaignimage").val($(this).find(".uploadLogo2 video").attr("src"));
			$("#campaignimagetype").val("video");
		}
		
		checkvalidation();
	});

}