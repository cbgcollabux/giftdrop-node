$("#logout").show();
$("#business-menu-campaign").addClass("active");

	var map;

	$(document).ready(function() {	
		$("#add-drop").click(function(){
			$("#tmp-add-drop").css("display","none");
			$("#tmp-with-panel").css("display","initial");
		});

		google.maps.event.addDomListener(window, 'load', init);
		
		
		$(".drop-rounded-header").click(function(){
			 	$(this).parent().find(".drop-rounded-body").show();
		 });

		 $(".drop-rounded-body a").click(function(){
		 	var text = $(this).text();
		 	$(this).parent().parent().find(".drop-rounded-header span").text(text);
		 	$(this).parent().parent().parent().find(".drop-rounded-header span").text(text);
			$(this).parent().parent().hide();
		 });

	});

	function DropControl(controlDiv, map) {
		var controlUI = document.createElement('div');
		controlUI.style.backgroundColor = '#fff';
		controlUI.style.cursor = 'pointer';
		controlUI.style.marginTop = '10px';
		controlUI.style.textAlign = 'center';
		controlDiv.appendChild(controlUI);


		var controlText = document.createElement('div');
		controlText.style.color = '#514F4F';
		controlText.style.fontFamily = 'GothamRndRegular';
		controlText.style.fontSize = '16px';
		controlText.style.lineHeight = '38px';
		controlText.style.paddingLeft = '15px';
		controlText.style.paddingRight = '15px';
		controlText.innerHTML = 'Drop Pin &nbsp;&nbsp; <img src="/img/green_pin.png">';
		controlUI.appendChild(controlText);

		controlUI.addEventListener('click', function() {
		  
		});
	}

	function codeAddress(drop){	
		console.log(drop);
	    var address = $("#gftnow-infobox").text();
	   
	    geocoder.geocode( { 'address': drop.address}, function(results, status){
	        if (status == google.maps.GeocoderStatus.OK){
	        	var overlay = new CustomMarker(
						results[0].geometry.location, 
						map,
						{},drop.quantity
				);

				var infowindow = new google.maps.InfoWindow({
		          content: document.getElementById("gftnow-infobox")
		         });

        		 google.maps.event.addListener(infowindow, 'domready', function() {
					var iwOuter = $('.gm-style-iw');
					var iwBackground = iwOuter.prev();
					iwBackground.children(':nth-child(2)').css({'display' : 'none'});
					iwBackground.children(':nth-child(4)').css({'display' : 'none'});
					iwBackground.children(':nth-child(1)').css({'display' : 'none'});
					iwBackground.children(':nth-child(3)').css({'display' : 'none'});
				});

        		 /*overlay.addListener(\'click\', function() {
		         	 infowindow.open(map, overlay);
		         });*/

				map.setCenter(results[0].geometry.location);
				
	            /*map.setCenter(results[0].geometry.location);
	            var marker = new google.maps.Marker(
	            {
	                map: map,
	                position: results[0].geometry.location,
	                icon: "http://localhost/~chanu/gftnow/assets/img/pin_with_num.png"
	            });*/
	        } else {
	        	$("#gftnow-alert-modal-content").text("Geocode was not successful for the following reason: " + status);
				$("#gftnow-alert-modal").modal("show");
	        }
	    });
	}

	function init() {

		geocoder = new google.maps.Geocoder();

        var mapOptions = {
            zoom: 12,

            center: new google.maps.LatLng(40.6700, -73.9400), // New York
            disableDefaultUI: true,
            mapTypeControl: true,
            streetViewControl: true,
            mapTypeControlOptions: {
            	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            },

            styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
        };

         var mapElement = document.getElementById('map');

         map = new google.maps.Map(mapElement, mapOptions);
		
		jQuery.ajax({
    		url: "/new_campaign-2/getSessionDrops",
    		type: "post",
    		dataType: "json",
    		success: function(data){
    			if(data.result=="success"){
    				var result;
    				for(var i=0;i<data.drops.length;i++){
    					codeAddress(data.drops[i]);
    				}
    			}
    		},error: function(err){
    			$("#gftnow-alert-modal-content").text(err.responseText);
				$("#gftnow-alert-modal").modal("show");
    		}
    	});
    }