$(document).ready(function() {

	$(".uploadLogo.uploadLogo2").each(function(){
		if($("#businesscategory").val() == $(this).attr("data-value") )
			$(this).addClass("active");
	});

	checkvalidation();

	function checkvalidation(){
		var err = 0;

		if( $("#businesscategory").val() == "" )
			err++;

		if(err==0){
			$("#submit").removeClass("gftnow-btn-default");
			$("#submit").addClass("gftnow-btn-success");
		}else{
			$("#submit").addClass("gftnow-btn-default");
			$("#submit").removeClass("gftnow-btn-success");
		}
	}

	$(".uploadLogo2").click(function(){
		$(".uploadLogo2").removeClass("active");
		$(this).addClass("active");
		$("#businesscategory").val( $(this).attr('data-value') );
		checkvalidation();
	});

	$("#form1").validate({
	 	submitHandler: function(form){
	 		if( $("#businesscategory").val() !='' ){
	 			jQuery.ajax({
	 				type: 'post',
	 				url: 'onboarding-1/submit',
	 				dataType: 'json',
	 				data: {category:$("#businesscategory").val()},
	 				success: function(data){
	 					$("#mask2").show();
	 					console.log(data);
	 					window.location = "onboarding-2";
	 				},
	 				error: function(err){
	 					console.log(err.responseText);
	 					$("#mask2").hide();
	 				}
	 			});
	 		}else{
	 			$("#gftnow-alert-modal-content").text("Please select business category");
				$("#gftnow-alert-modal").modal("show");
	 		}
	 	}
	});

	$(".uploadLogo.uploadLogo2[data-value='"+$("#businesscategory").val()+"']").addClass("active");

});