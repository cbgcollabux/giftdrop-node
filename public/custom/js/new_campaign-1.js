$("#logout").show();
var map;
var markers = new Array();
var barcodes = new Array();

function uniqid (prefix, more_entropy) {
	if (typeof prefix === 'undefined')
		prefix = "";

	var retId;
	var formatSeed = function (seed, reqWidth) {
		seed = parseInt(seed, 10).toString(16); // to hex str
		if (reqWidth < seed.length) { // so long we split
			return seed.slice(seed.length - reqWidth);
		}
		if (reqWidth > seed.length) { // so short we pad
			return Array(1 + (reqWidth - seed.length)).join('0') + seed;
		}
		return seed;
	};

	// BEGIN REDUNDANT
	if (!this.php_js) {
		this.php_js = {};
	}
	// END REDUNDANT
	if (!this.php_js.uniqidSeed) { // init seed with big random int
		this.php_js.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
	}
	this.php_js.uniqidSeed++;

	retId = prefix; // start with prefix, add current milliseconds hex string
	retId += formatSeed(parseInt(new Date().getTime() / 1000, 10), 8);
	retId += formatSeed(this.php_js.uniqidSeed, 5); // add seed hex string
	if (more_entropy) {
		// for more entropy we add a float lower to 10
		retId += (Math.random() * 10).toFixed(8).toString();
	}
  	return retId;
}

function generateQuickGuid() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}

function executeDropEvent(id){
	var qty = $("#quantity[data-id=\'"+id+"\']").val();
	var address = $("#text-"+id).val();
	
	if(address==""){
		$("#gftnow-alert-modal-content").html("Please enter address");
		$("#gftnow-alert-modal").css("position", "fixed").modal("show");
	}else{

		var rowcounter = $(".rowcounter").length;

		var row = "<div class=\'row drop-row-container\' data-id=\'"+id+"\' style=\'margin-bottom:-10px;\'><div class=\'col-md-7 gftnow-nopadding txt-address-container\'><p style=\'display:inline-block;width: 30px; margin-left:41px;\' class=\'rowcounter\'>2</p><input class=\'drops\' type=\'hidden\' id=\'text-"+id+"\' value=\'"+address+"\'><input type=\'hidden\' name=\'lat-"+id+"\' id=\'lat-"+id+"\'><input type=\'hidden\' name=\'lng-"+id+"\' id=\'lng-"+id+"\'><input type=\'hidden\' id=\'qty-"+id+"\'><input class=\'form-control txt-address-clicked\' data-id=\'"+id+"\' disabled style=\'display:inline;width: 80%;\' placeholder=\'Type address here\' id=\'txt-address-clicked-"+id+"\' type=\'text\' value=\'"+address+"\'></div><div class=\'col-md-1 text-left\'><p style=\'padding-top: 5px;\'>Quantity</p></div><div id=\'quantityDiv\' class=\'col-md-2\'>&nbsp;&nbsp;&nbsp;<input style=\'display:inline-block;width:50px\' id=\'quantity\' data-id=\'"+id+"\' class=\'form-control text-center quantity\' disabled value=\'"+qty+"\' type=\'text\'>&nbsp;&nbsp;</div><div class=\'col-md-2 text-right\'><i style=\'cursor:pointer;font-size: 28px; color: #E3B338;\' class=\'fa fa-pencil edit-row\' data-id=\'"+id+"\'></i> <i style=\'cursor:pointer;font-size: 28px; color: #E3B338;\' class=\'fa fa-trash-o remove-row\' data-id=\'"+id+"\'></i></div></div>";

		$(".barcodes-div").remove();

		row += "<div class=\'row\' style=\'margin-top:20px;\'><div class=\'col-md-8\'>"+
				"<div id=\'generateBarcodes\' style=\'left:0;right:0;margin:0 auto;\' class=\'gftnow-panel gftnow-hide barcodes-div\'>"+
					"<div class=\'pull-right\'><a href=\'javascript:void(0);\' id=\'close-gB\'><i class=\'fa fa-times\'></i></a></div>"+
					"<br/>"+
					"<center>"+
						"<b>Bar Codes</b> <br/>"+
						"Enter your generated barcodes manually"+
						"<br/><br/>"+
						"<label type=\'button\' style=\'display:none;\' id=\'generateBarcodesBtn\' class=\'btn btn-success gftnow-btn-rounded\'>Generate</label>"+
						"<div id=\'generatedBarcodes\'></div>"+
					"</center>"+
				"</div>"+

				"<div id=\'uploadBarcodes\' class=\'gftnow-panel gftnow-hide barcodes-div\'>"+
					"<div class=\'pull-right\'><a href=\'javascript:void(0);\' id=\'close-uB\'><i class=\'fa fa-times\'></i></a></div>"+
					"<br/>"+
					"<center>"+
						"<b>Bar Codes</b> <br/>"+
						"Enter your generated barcodes manually"+
						"<br/><br/>"+
						"<label type=\'button\' id=\'uploadBarcodesBtn\' class=\'btn btn-success gftnow-btn-rounded\'>Upload from file</label>"+
						"<div id=\'uploadedBarcodessample\'>"+
							"<br/><br/>"+
							"<div style=\'overflow-y:auto;height:240px;\'>"+
								"<table class=\'table\' id=\'gftnow-barcodes-table\'>"+
									"<tr>"+
										"<td>1</td>"+
										"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
										"<td></td>"+
									"</tr>"+
									"<tr>"+
										"<td>2</td>"+
										"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
										"<td></td>"+
									"</tr>"+
									"<tr>"+
										"<td>3</td>"+
										"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
										"<td></td>"+
									"</tr>"+
									"<tr>"+
										"<td>4</td>"+
										"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
										"<td></td>"+
									"</tr>"+
									"<tr>"+
										"<td>5</td>"+
										"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
										"<td></td>"+
									"</tr>"+
									"<tr>"+
										"<td>6</td>"+
										"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
										"<td></td>"+
									"</tr>"	+														
								"</table>"+
							"</div>"+
							"<br/><br/>"+
							"<a href=\'javascript:void(0)\' class=\'doneBtn btn btn-default gftnow-btn gftnow-btn-default\'>Done</a>"+											
						"</div>"+
						"<div id=\'uploadedBarcodestable\'></div>"+
					"</center>"+
				"</div></div></div>";

		$(".add-drop[data-id=\'"+id+"\']").parent().parent().html(row);

		$(".edit-row[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
			editRow($(this));
		});

		var addressPicker = new AddressPicker();

		$("#txt-address-clicked-"+id).typeahead(null, {
		  displayKey: 'description',
		  source: addressPicker.ttAdapter()
		});

		addressPicker.bindDefaultTypeaheadEvent( $("#txt-address-clicked-"+id) );

		$(addressPicker).on('addresspicker:selected', function (event, result) {
		  	$('#text-'+id).val(result.placeResult.formatted_address);
		});

		$("#txt-address-clicked-"+id).change(function(){
			$('#text-'+id).val($(this).val());
		});

		$("#row-counter-container").fadeIn("slow");

		$(".rowcounter").each(function(i,f){
			$(this).text(i+1);
		});

		$(".remove-row").unbind("click").bind("click", function(){
			//$(this).parent().parent().parent().parent().parent().remove();
			var id = $(this).attr('data-id');
			$(".drop-row-container[data-id='"+id+"']").parent().remove();

			for(var i =0;i<markers.length;i++){
				if(markers[i].get("id")==$(this).attr("data-id")){
					markers[i].setMap(null);
					markers.splice(i,1);
				}
			}

			$(".rowcounter").each(function(i,f){
				$(this).text(i+1);
			});

			checkvalidation();
		});


		$(".add_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
			 	var qty = $(this).parent().find("#quantity");
			 	var eid = $(this).attr("data-id");
			    var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );
		        $(".quantity[data-id=\'"+eid+"\']").val( currentVal+1 );

		        checkvalidation();
		       
		    });

		    $(".minus_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
		    	var eid = $(this).attr("data-id");
		        var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );

		       	if(currentVal>1)
					$(".quantity[data-id=\'"+eid+"\']").val(currentVal-1)

				checkvalidation();
		      
		    });


		$("#close-gB").unbind("click").bind("click", function(){
			$("#generateBarcodes").hide();
		});
		$("#close-uB").unbind("click").bind("click", function(){
			$("#uploadBarcodes").hide();
		});

		console.log("address "+address);
		console.log("id "+id);
		codeAddress(address, id);				

		if($("#type").val() == "gift-card"){
			generateBarcodes();
			$(".barcodes-div").hide();
		}

	}

	setTimeout(function(){  checkvalidation(); }, 1000);
}

function addDropEvent(elem){
	$(elem).click(function(){
		//var address = $(this).parent().parent().find(".txt-address-container").find("input[id*=text-]").val();
		var id = $(elem).attr('data-id');
		executeDropEvent(id);
		$(".edit-row").unbind("click").bind("click",function(){
			editRow($(this));
			$(".edit-drop").unbind("click").bind("click",function(){
				editDrop($(this));
			});
		});
	});
}

function editRow(elem){
	var id = $(elem).attr("data-id");

	var qty = $("#quantity[data-id=\'"+id+"\']").val();
	$("#quantity[data-id=\'"+id+"\']").parent().html("<i class=\'fa fa-chevron-left minus_qty\' data-id=\'"+id+"\'></i>&nbsp;<input style=\'display:inline-block;width:50px;\' id=\'quantity\' data-id=\'"+id+"\' class=\'form-control text-center quantity\' value=\'"+qty+"\' type=\'text\'>&nbsp;<i class=\'fa fa-chevron-right add_qty\' data-id=\'"+id+"\'></i>");
	
	$(".add_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
	 	var qty = $(this).parent().find("#quantity");
	 	var eid = $(this).attr("data-id");
	    var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );
        $(".quantity[data-id=\'"+eid+"\']").val( currentVal+1 );

        checkvalidation();
       
    });

    $(".minus_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
    	var eid = $(this).attr("data-id");
        var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );

       	if(currentVal>1)
			$(".quantity[data-id=\'"+eid+"\']").val(currentVal-1)

		checkvalidation();
    });

	$("#txt-address-clicked-"+id).prop("disabled",false);
	
	$(elem).parent().html("<button class=\'btn btn-success btn-block gftnow-hide edit-drop\' data-qty=\'1\' data-id=\'"+id+"\' style=\'display: inline-block;\'>Save</button>");
	
	$(".edit-drop[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
		editDrop($(this));
	});

	checkvalidation();
	
}

function editDrop(elem){
	var id = $(elem).attr("data-id");
	$("#txt-address-clicked[data-id=\'"+id+"\']").prop("disabled",true);
	$("#quantity[data-id=\'"+id+"\']").parent().html("&nbsp;&nbsp;&nbsp;<input style=\'display:inline-block;width:50px;\' id=\'quantity\' disabled data-id=\'"+id+"\' class=\'form-control text-center quantity\' value=\'"+$('#quantity[data-id="'+id+'"]').val()+"\' type=\'text\'>");
	$(elem).parent().html("<i style=\'cursor:pointer;font-size: 28px; color: #E3B338;\' class=\'fa fa-pencil edit-row\' data-id=\'"+id+"\'></i> <i style=\'cursor:pointer;font-size: 28px; color: #E3B338;\' class=\'fa fa-trash-o remove-row\' data-id=\'"+id+"\'></i>");

	$(".edit-row[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
		editRow($(this));
	});

	$(".add_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
	 	var qty = $(this).parent().find("#quantity");
	 	var eid = $(this).attr("data-id");
	    var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );
        $(".quantity[data-id=\'"+eid+"\']").val( currentVal+1 );

        checkvalidation();
       
    });

    $(".minus_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
    	var eid = $(this).attr("data-id");
        var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );

       	if(currentVal>1)
			$(".quantity[data-id=\'"+eid+"\']").val(currentVal-1)

		checkvalidation();
      
    });

    var qty = $("#quantity[data-id=\'"+id+"\']").val();
	var address = $("#text-"+id).val();

	for(var i =0;i<markers.length;i++){
		if(markers[i].get("id")==id){
			markers[i].setMap(null);
			markers.splice(i,1);
		}
	}

	codeAddress(address, $(elem).attr("data-id"));

	setTimeout(function(){  checkvalidation(); }, 1000);

	if($("#type").val() == "gift-card"){
		generateBarcodes();
	}
}
function checkvalidation(){
	
	var x = 0;

	if($("input[name=\'offer\']").val() == "")
		x++;
	if($("input[name=\'date1\']").val() == "")
		x++;

	if($("input[name=\'time1\']").val() == "")
		x++;

	if($("textarea[name=\'redemption_details\']").val() == "")
			x++;

	if(markers.length==0)
		x++;

	/*if( $("#vendor").val() == -1 || $("#vendor").val() =="-1" ){
		console.log("im here");
		console.log($("#vendor").val());
		
	}*/
	if(barcodes.length==0)
			x++;

	if($(".edit-drop").length > 0)
		x++;
		
	if(x==0){
		$("#submit").removeClass("gftnow-btn-default");
		$("#submit").addClass("gftnow-btn-success");
	}else{
		$("#submit").addClass("gftnow-btn-default");
		$("#submit").removeClass("gftnow-btn-success");
	}

	console.log(x);
}

checkvalidation();

$(document).ready(function() {

	if($("#barcodes").val()!=""){
		barcodes = $("#barcodes").val().split(",");
	}

	checkvalidation();

	if(!google && !google.maps){
	    $("#map-error").show();
	}

	var dropstring = $("#drops").val();
	//$("#redemption_details").val($("#redemption_details").val());
	/*if(JSON.parse(dropstring)){
		var drops = JSON.parse(dropstring);
		console.log(drops);

		$("#tmp-add-drop").html("");
		for(var i = 0 ; i< drops.length;i++){
			var html = "<div class='row'>"+
				"<div class='col-md-7 gftnow-nopadding txt-address-container'>"+
					"<p class='rowcounter' style='display:inline-block;width:30px;margin-left:27px;'>"+(i+1)+"</p>"+
				"</div>"+
			"</div>";
			$("#tmp-add-drop").append("<div class='row'>""</div>");
		}
	}*/

	google.maps.event.addDomListener(window, 'load', init);

	var vendor = $("#vendor").val();

	$("#key5").val(uniqid());

	$("#offer-minus").click(function(){
		var vendor = $("#vendor").val();
		
		if(vendor!=-1){
			var gftamount = $("#gftamount").val();
			var amounts = gftamount.split(',');
			var dataIndex = Number($("#offer").attr("data-index"));

			if(dataIndex>0){
				dataIndex--;
				$("#offer").val(amounts[dataIndex]);
				$("#offer").attr("data-index", dataIndex);

				if(dataIndex==0){
					$("#offer-minus").removeClass("gftnow-btn-success").addClass("gftnow-btn-default");
					$("#offer-plus").removeClass("gftnow-btn-default").addClass("gftnow-btn-success");
				}
			}
		}else{
			$(this).blur();
	        var qty = $("#offer");
	        var currentVal = parseInt(qty.val());
	        var newVal='';
	       	if(currentVal>1){
	       		newVal = currentVal-1;
	            qty.val(newVal);
	        }
	        if(currentVal<=1){
				newVal = currentVal;
	            qty.val(newVal);
	        	$(this).removeClass("gftnow-btn-success").addClass("gftnow-btn-default");
	        }
		}
	});

	$("#offer-plus").click(function(){
		var vendor = $("#vendor").val();
		
		if(vendor!=-1){
			var gftamount = $("#gftamount").val();
			var amounts = gftamount.split(',');
			var dataIndex = Number($("#offer").attr("data-index"));

			if(dataIndex<amounts.length-1){
				dataIndex++;
				$("#offer").val(amounts[dataIndex]);
				$("#offer").attr("data-index", dataIndex);

				if(dataIndex==amounts.length-1){
					$("#offer-plus").removeClass("gftnow-btn-success").addClass("gftnow-btn-default");
					$("#offer-minus").removeClass("gftnow-btn-default").addClass("gftnow-btn-success");
				}
			}
		}else{
			$(this).blur();
		 	var qty = $("#offer");
	        var currentVal = parseInt(qty.val());
	        var newVal = currentVal+1;
	        qty.val(newVal);
	        if(currentVal>=1)
	        	$("#offer-minus").removeClass("gftnow-btn-default").addClass("gftnow-btn-success");
		}
	});

	$("#offer").keydown(function (e) {
	    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
	        (e.keyCode >= 35 && e.keyCode <= 40)) {
	             return;
	    }
	    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	        e.preventDefault();
	    }
	});
	
	if(vendor!=-1){
		var gftamount = $("#gftamount").val();
		var amounts = gftamount.split(',');
		$("#offer").val(amounts[0]);
		$("#offer").attr("data-index", 0);
		$("#offer").attr("data-max-index", amounts.length);
	}

	if($("#barcodetype").val() == "Generated"){
		$("#checkboxBarcodes").addClass("active");
		$("#checkboxAddManually").removeClass("active");
	}

	if($("#barcodetype").val() == "Manually Added"){
		$("#checkboxAddManually").addClass("active");
		$("#checkboxBarcodes").removeClass("active");		
	}

	$("input, textarea").keyup(function(){
		checkvalidation();
	});

	$("#form").validate({
		rules: {
			offer: "required",
			date1: "required",
			time1: "required",
			redemption_details: "required"
		},errorPlacement: function(){
			return false;
		},submitHandler: function(form){
			var drops = new Array();

			$(".drops").each(function()
			{
				if($(this).val()!=""){
					var drop = {};
					drop.address = $(this).val();
					drop.quantity = $(this).parent().parent().find("#quantity").val();
					drop.lat = $(this).attr("lat");
					drop.lng = $(this).attr("lng");
					drops.push(drop);
				}
					
			});

			console.log("drops");
			console.log(drops);

			console.log("barcodes");
			console.log(barcodes);
			

			var err=0;
			var errMessages="";
			if(barcodes.length==0){

				if($("#gftvendor").val()=="-1"||$("#gftvendor").val()==-1){
					err++;
					errMessages+="You forgot to generate barcodes<br/>";
				}else{
					var totalb=0;
					$(".quantity").each(function(){
						
						totalb = totalb+Number($(this).val())						
					});

					for(var i =0;i<totalb;i++){
						barcodes.push("");
					}
				}
				
			}

			if($("#offer").val() == "0" || $("#offer").val() == ""){
				err++;
				errMessages+="Offer item value should be more than 0.<br/>";
			}
			if(drops.length==0){
				err++;
				errMessages+="At least create 1 drop<br/>";
			}
			if($("#type").val() == "free-product"){
				if($("#checkboxBarcodes").hasClass("active") || $("#checkboxAddManually").hasClass("active")){
					
				}else{
					err++;
					errMessages+="Generate Barcodes or Add it Manually.<br/>";
				}
			}

			if(err!=0){
				$("#gftnow-alert-modal-content").html(errMessages);
				$("#gftnow-alert-modal").modal("show");
			}else{
				$("#drops").val( JSON.stringify(drops));
				$("#barcodes").val( barcodes.toString());

				if($("#submit").hasClass("gftnow-btn-default")){

				}else{
					$("#mask2").show();
					form.submit();
				}
				
			}
		}
	});

	$("#time1").timepicker('setTime', new Date());

	/*new start*/
	$("#add-new-btn2").click(function(){
		addNewDrop();
	});
	/*new end*/

	$(".txt-address").focus(function(){
		$(this).parent().parent().find(".add-drop").fadeIn("slow",function(){
			
		});
	});

	//$("#generateBarcodes").hide();

	$("#checkboxBarcodes").click(function(){
		$("#barcodetype").attr("value", "Generated");
		$("#uploadBarcodes").hide();
		$(this).addClass("active");
		$("#checkboxAddManually").removeClass("active");

		if(markers.length==0){
			$("#gftnow-alert-modal-content").html("At least create 1 drop to generate barcodes.");
			$("#gftnow-alert-modal").modal("show");

			$("#checkboxAddManually").removeClass("active");
			$("#checkboxBarcodes").removeClass("active");
		}else{
			generateBarcodes();
		}
	});

	$("#checkboxAddManually").click(function(){
		$("#barcodetype").attr("value", "Manually Added");
		$("#generateBarcodes").hide()
		$(this).addClass("active");
		$("#checkboxBarcodes").removeClass("active");

		if(markers.length==0){
			$("#gftnow-alert-modal-content").html("At least create 1 drop to generate barcodes.");
			$("#gftnow-alert-modal").modal("show");

			$("#checkboxAddManually").removeClass("active");
			$("#checkboxBarcodes").removeClass("active");
		}else{
			uploadBarCodes();
		}
	});

	$("#generateBarcodesBtn").click(function(){
	    generateBarcodes();
	});

	var date = new Date();
	date.setDate(date.getDate());

	$('#date1').datepicker({
		 startDate: date
	});

	$('#date1').on('changeDate', function(ev){
	    $(this).datepicker('hide');
	});
	
	$(".drop-rounded-header").click(function(){
		 	$(this).parent().find(".drop-rounded-body").show();
	 });

	 $(".drop-rounded-body a").click(function(){
	 	var text = $(this).text();
	 	$(this).parent().parent().find(".drop-rounded-header span").text(text);
	 	$(this).parent().parent().parent().find(".drop-rounded-header span").text(text);
		$(this).parent().parent().hide();
	 });

});

function addNewDrop(drop){
	console.log("drops");
	console.log( drop );

	var id = generateQuickGuid();
	var rowcounter = $(".rowcounter").length;
	var row = '<div class="row">'+
					'<div class="col-md-12">'+
						'<div class="row">'+
							'<div class="col-md-7 gftnow-nopadding txt-address-container">'+
								'<p style="display:inline-block;width: 30px;margin-left:27px;" class="rowcounter">'+rowcounter+'</p>'+
								'<input type="hidden" class="drops" id="text-'+id+'" name="text-'+id+'" value="'+( (drop)?drop.address:"" )+'"/><input type="hidden" class="lat" id="lat-'+id+'" value="'+( ( drop )? drop.lat:"" )+'"/><input class="lng" type="hidden" id="lng-'+id+'" value="'+( (drop ) ? drop.lng:"" )+'"/><input style="display:inline;width: 83%;" class="form-control txt-address" placeholder="Type address here" id="'+id+'" type="text" value="'+( ( drop )? drop.address:"" )+'"/>'+
							'</div>'+
							'<div class="col-md-1 text-left">'+
								'<p class="qtyTxt" style="padding-top: 5px;">Quantity</p>'+
							'</div>'+
							'<div class="col-md-2">'+
								'<div id="quantityDiv" class="qtyDiv">'+
									'<i class="fa fa-chevron-left minus_qty" data-id="'+id+'"></i>&nbsp;<input style="display:inline-block;width:50px;" id="quantity" data-id="'+id+'" class="form-control text-center quantity" value="'+( ( drop )?drop.quantity:"1" ) +'" type="text">&nbsp;<i class="fa fa-chevron-right add_qty" data-id="'+id+'"></i>'+
								'</div>'+
							'</div>'+
							'<div class="col-md-2">'+
								'<button type="button" class="btn btn-success btn-block add-drop" data-id="'+id+'">Drop on Map</button>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';

	$("#tmp-add-drop").append(row);

	if(drop){
		
		executeDropEvent(id);
	}

	var addressPicker = new AddressPicker();

	$("#"+id).typeahead(null, {
	  displayKey: 'description',
	  source: addressPicker.ttAdapter()
	});

	addressPicker.bindDefaultTypeaheadEvent($('#'+id));

	$(addressPicker).on('addresspicker:selected', function (event, result) {
	  	$('#text-'+id).val(result.placeResult.formatted_address);
	});

	$("#"+id).change(function(){
		$('#text-'+id).val($(this).val());
	});

	$(".add_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
	 	var qty = $(this).parent().find("#quantity");
	 	var eid = $(this).attr("data-id");
	    var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );
        $(".quantity[data-id=\'"+eid+"\']").val( currentVal+1 );

        checkvalidation();
       
	});

    $(".minus_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
    	var eid = $(this).attr("data-id");
        var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );

       	if(currentVal>1)
			$(".quantity[data-id=\'"+eid+"\']").val(currentVal-1)

		checkvalidation();
      
    });

	$(".txt-address").focus(function(){
		var qty = $(this).parent().parent().parent().parent().find("#quantity");
		$(this).parent().parent().parent().find(".qtyTxt").fadeIn("slow");
		$(this).parent().parent().parent().find(".qtyDiv").fadeIn("slow");
		$(this).parent().parent().parent().find(".add-drop").fadeIn("slow", function(){
			$(this).attr("data-qty", parseInt(qty.val()));
			$(this).attr("data-id", id);
		});
	});
	
	addDropEvent( $(".add-drop[data-id=\'"+id+"\']") );

	$(".rowcounter").each(function(i,f){
		$(this).text(i+1);
	});

	$("#row-counter-container").fadeOut("fast");

	$("#text-"+id).keyup(function () {
	   if ($(this).val()) {
	      $(".add-drop").show();
	   }
	   else {
	      $(".add-drop").hide();
	   }
	});

	checkvalidation();
}

function fileupload(){
	/*var url = "assets/upload/php/index.php";
	var cropted = "";

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        add: function(e,data){
        	var uploadErrors = [];
        	var acceptFileTypes = /(\.|\/)(csv|plain)$/i;

        	
        	if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
	            uploadErrors.push('Not an accepted file type. Please upload csv or text file only');
	        }
	        if(uploadErrors.length > 0) {
	            $("#gftnow-alert-modal-content").html(uploadErrors.join("\n"));
				$("#gftnow-alert-modal").modal("show");
	        } else {
	            data.submit();
	        }
        },
        done: function (e, data) {

      
        	if(data.result.files.length > 0){

        		jQuery.ajax({
        			url: "mvc/controller/ajaxController.php",
        			type: "post",
        			dataType: "json",
        			data: {func:"readBarcodes",url: data.result.files[0].name,type: data.result.files[0].type},
        			success: function(data){
        				
        				if(data.result=="OK"){

        					var barcodestring = "";
        					barcodes = new Array();
							for(var i =0; i<data.barcodes.length; i++){
								barcodes.push(data.barcodes[i]);
								barcodestring+="<tr><td>"+(i+1)+"</td><td>"+data.barcodes[i]+"</td><td><img src=\'assets/img/mini-check.png\'></td></tr>";
							}
							
							$("#uploadedBarcodessample").hide();
							$("#uploadedBarcodestable").html("<br/><br/><div id=\'gftnow-scrollbar\' style=\'overflow-y:scroll;height:auto;max-height:240px;\'><table class=\'table\' id=\'gftnow-barcodes-table\'>"+barcodestring+"</table></div><br/><br/><a href=\'javascript:void(0)\' class=\'doneBtn btn btn-success gftnow-btn-rounded\'>Done</a>");		
							
							$("#uploadBarcodes .doneBtn").unbind("click").bind("click",function(e){
								
								$("#uploadBarcodes").hide();
								checkvalidation();
								
							});
        				}
        			},error: function(err){
        				
        				$("#gftnow-alert-modal-content").html(err.responseText);
						$("#gftnow-alert-modal").modal("show");
        			}
        		});

            	$("#mask2").hide();
            	$("#mask1").show();
            }
            
        },
        error: function(err){
        	console.log("error");
        	console.log(err.responseText);
        },
        progressall: function (e, data) {
        	console.log("uplaoding");
          
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

	checkvalidation();*/
}

function uploadBarCodes(){
	//$("#generatedBarcodes").hide();
	/*$("#uploadBarcodes").show();
	fileupload();
	$("#uploadBarcodesBtn").unbind("click").bind("click",function(){
		$("#fileupload").click();
	});

	checkvalidation();*/
}

function generateBarcodes(){
	var charset = "0123456789";
	var quantity = 0;
	
	 $(".quantity").each(function(){
		quantity += Number($(this).val());
	  });

	barcodes = new Array();
	var barcodestring = "";
	for(var i =0; i<quantity-1; i++){
		var barcode = generateRandom(5)+"-"+generateRandom(5)+"-"+generateRandom(7);
		barcodes.push(barcode);
		if($("#type").val()=="free-product"){
			barcodestring+="<tr><td>"+(i+1)+"</td><td>"+barcode+"</td><td><img src=\'/img/mini-check.png\'></td></tr>";
		}
	}

	console.log(barcodes);

	//if($("#type").val()=="free-product"){
		$("#generatedBarcodes").html("<br/><br/><div id=\'gftnow-scrollbar\' style=\'overflow-y:scroll;height:auto;max-height:240px;\'><div class=\'pull-right\'><a href=\'javascript:void(0);$(\'#generatedBarcodes\').hide();\'></a></div><table class=\'table\' id=\'gftnow-barcodes-table\'>"+barcodestring+"</table></div><br/><br/><a href=\'javascript:void(0)\' class=\'doneBtn btn btn-success gftnow-btn-rounded\'>Done</a>");
		$("#generateBarcodes").show();

		$("#generateBarcodesBtn").unbind("click").bind("click",function(){
		    generateBarcodes();
		});

		$("#generateBarcodes .doneBtn").unbind("click").bind("click", function(e){
			$("#generateBarcodes").hide();
			$("#uploadBarcodes").hide();
			checkvalidation();
		});
	//}
}

function generateRandom(length){
	var charset = "0123456789";
	var string = "";
	for(var i=0;i<length;i++){
		string+=charset.charAt(Math.floor(Math.random() * charset.length));
	}
	return string;
}

function DropControl(controlDiv, map) {
    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginTop = '10px';
    controlUI.style.textAlign = 'center';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = '#514F4F';
    controlText.style.fontFamily = 'GothamRndRegular';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '15px';
    controlText.style.paddingRight = '15px';
    controlText.innerHTML = 'Drop Pin &nbsp;&nbsp; <img src="/img/green_pin.png">';
    controlUI.appendChild(controlText);

	// Setup the click event listeners: simply set the map to Chicago.
	controlUI.addEventListener('click', function() {
		//add event here
		if( $("#row-counter-container").is(":visible") ){
			$("#add-new-btn2").click();
		}else{
			$("#gftnow-alert-modal-content").html("Please enter address");
			$("#gftnow-alert-modal").modal("show");
		}	

	});

    checkvalidation();
}


function init(){
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 12,

        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(40.6700, -73.9400), // New York
        disableDefaultUI: true,
        mapTypeControl: true,
        streetViewControl: true,
        mapTypeControlOptions: {
        	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },


        // How you would like to style the map. 
        // This is where you would paste any style found on Snazzy Maps.
        styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
    };

    // Get the HTML DOM element that will contain your map 
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('map');

    // Create the Google Map using our element and options defined above
     map = new google.maps.Map(mapElement, mapOptions);

    var dropControlDiv = document.createElement('div');
	var dropControl = new DropControl(dropControlDiv, map);

	dropControlDiv.index = 1;
	map.controls[google.maps.ControlPosition.TOP_CENTER].push(dropControlDiv);

	checkvalidation();

	if( $("#sessiondrops").length>0 ){
		var sessionDrops = JSON.parse( $("#sessiondrops").val() );

		if(sessionDrops){
			for(var i = 0; i<sessionDrops.drops.length; i++){
				addNewDrop( sessionDrops.drops[i] );
			}
		}
	}else{
		$.getJSON('https://ipinfo.io/geo', function(response) { 
		    var loc = response.loc.split(',');8 
		    var coords = {
		        latitude: loc[0],
		        longitude: loc[1]
		    };

		    var initialLocation = new google.maps.LatLng(coords.latitude, coords.longitude);
		    map.setCenter(initialLocation);
		});
	}
	$("#add-new-btn2").click();
}

function codeAddress(address,id){
	console.log("codeAddress");
	console.log(address);
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status){
        if (status == google.maps.GeocoderStatus.OK){
        	$("#text-"+id).attr("lat", results[0].geometry.location.lat());
	        $("#text-"+id).attr("lng", results[0].geometry.location.lng());
	        $("#lat-"+id).val(results[0].geometry.location.lat());
	        $("#lng-"+id).val(results[0].geometry.location.lng());

            map.setCenter(results[0].geometry.location);
           
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                icon: "/img/icon-pin.png",
                draggable: true,
				animation: google.maps.Animation.DROP
            });

            google.maps.event.addListener(marker, "dragend", function(event) { 
				var lat = event.latLng.lat(); 
				var lng = event.latLng.lng();

				$("#text-"+id).attr("lat", lat);
				$("#text-"+id).attr("lng", lng);
				$("#lat-"+id).val(lat);
				$("#lng-"+id).val(lng);

				var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};

				geocoder.geocode({'location': latlng}, function(results, status) {
					if (status === 'OK') {
					    if (results[1]) {
					    var id = marker.get("id");
					    $("#text-"+id).val(results[1].formatted_address);
					    $("#text-"+id).parent().find("#txt-address-clicked-"+id).val(results[1].formatted_address);
					    
					    } else {
					     console.log('No results found');
					    }
					} else {
					    console.log('Geocoder failed due to: ' + status);
					}
				});
	        }); 

            marker.set("id",id);
            map.setCenter(marker.getPosition());
            markers.push(marker);
        } else {
        	$("#gftnow-alert-modal-content").text("Geocode was not successful for the following reason: " + status);
			$("#gftnow-alert-modal").modal("show");
        }
    });
	checkvalidation();
}