$(document).ready(function(){
	/*console.log("hello world");
	if($("#api_error").length>0){
		console.log($("#api_error").length);
		$("#gftnow-alert-modal-content").html($("#api_error").val());
		$("#gftnow-alert-modal").css({ "position":"fixed","left":0,"right":0,margin: "0 auto" }).modal("show");
		
	}else{
		console.log("wadada");
	}*/

	$("#logout").show();
	$("#business-menu-campaign").addClass("active");

	$("#drop-rounded-header-1").unbind("click").bind("click", function(){
		$("#drop-rounded-body-1").slideToggle("fast");
	});

	$("#drop-rounded-header-2").unbind("click").bind("click", function(){
		$("#drop-rounded-body-2").slideToggle("fast");
	});

	$("#new-campaign-disabled").click(function(){
		$("#gftnow-alert-modal-content").html("Cannot add new campaign because your company is still pending for approval.");
		$("#gftnow-alert-modal").modal("show");
	});

	$(".selectpicker_").selectpicker();
});