$(document).ready(function() {
	init();
	movePinUp();
	moveSMelipseRight();
	rotateBGelipse();
    fadeInOut(jQuery('#text-slider li:first-child'));
	moveMouseScrollUp();
	$("#button-scroll-click").click(function() {
	    $("html, body").animate({
	        scrollTop: $("#gftnow-landing-discover").offset().top
	    }, 1000);
	});
});

function moveMouseScrollUp(){
    $("#moving-mousescroll").animate({top: "+=20"}, 500, moveMouseScrollDown);
}
function moveMouseScrollDown(){
    $("#moving-mousescroll").animate({top: "-=20"}, 500, moveMouseScrollUp);
}

function fadeInOut(item) {
	item.fadeIn(1000).delay(3000).fadeOut(1000, function() {
		if (item.next().length) // if there is a next element
		{
			fadeInOut(item.next());
		} // use it
		else {
			fadeInOut(item.siblings(':first'));
		} // if not, then use go back to the first sibling
	});
}

(function() {
    var paper, circs, i, nowX, nowY, timer, props = {}, toggler = 0, elie, dx, dy, cur;
    // Returns a random integer between min and max  
    // Using Math.round() will give you a non-uniform distribution!  
    function ran(min, max){  
        return Math.floor(Math.random() * (max - min + 1)) + min;  
    } 
    
    function moveIt(){
        for(i = 0; i < circs.length; ++i){            
			// Reset when time is at zero
            if (! circs[i].time) 
            {
                circs[i].time  = ran(30, 100);
                circs[i].deg   = ran(-179, 180);
                circs[i].vel   = ran(1, 5);  
                circs[i].curve = ran(0, 1);
            }                
			// Get position
            nowX = circs[i].attr("x");
            nowY = circs[i].attr("y");   
			// Calc movement
            dx = circs[i].vel * Math.cos(circs[i].deg * Math.PI/180);
            dy = circs[i].vel * Math.sin(circs[i].deg * Math.PI/180);
			// Calc new position
            nowX += dx;
            nowY += dy;
			// Calc wrap around
            if (nowX < 0){
            	if (window.matchMedia('(max-width: 767px)').matches){
            		nowX = 380 + nowX;
            	}else{
            		nowX = 630 + nowX;
            	}
            }
            else{
            	if (window.matchMedia('(max-width: 767px)').matches){
            		nowX = nowX % 380;
            	}else{
            		nowX = nowX % 630;            
            	}
            }
            if (nowY < 0){
            	nowY = 480 + nowY;
            }
            else{
            	nowY = nowY % 480;
            }
            
			// Render moved particle
            circs[i].attr({x: nowX, y: nowY});

			// Calc curve
            if (circs[i].curve > 0) circs[i].deg = circs[i].deg + 2;
            else                    circs[i].deg = circs[i].deg - 2;

            // Progress timer for particle
            circs[i].time = circs[i].time - 1;
            
			// Calc damping
            if (circs[i].vel < 1) circs[i].time = 0;
            else circs[i].vel = circs[i].vel - .05;              
       
        } 
        timer = setTimeout(moveIt, 60);
    }
    
    window.onload = function () {
    	var sizeRand, imgRand, imgURL, width, height;
    	if (window.matchMedia('(max-width: 768px)').matches){
    		paper = Raphael("canvas", 400, 700);
    	}else{
        	paper = Raphael("canvas", 650, 700);
    	}
        circs = paper.set();
        for (i = 0; i < 20; ++i){
        	sizeRand = ran(1,5);
        	if(sizeRand == 1 || sizeRand == 2){
        		sizeWH = 250;
        		imgRand = ran(1,4);
        		if(imgRand == 1)
        			imgURL = "img/bubble-lg-blue.png";
        		if(imgRand == 2)
        			imgURL = "img/bubble-lg-green.png";
    			if(imgRand == 3)
    				imgURL = "img/bubble-lg-purple.png";
    			if(imgRand == 4)
    				imgURL = "img/bubble-lg-yellow.png";
        	}
        	if(sizeRand == 3 || sizeRand == 4){
        		sizeWH = 75;
        		imgRand = ran(1,4);
        		if(imgRand == 1)
        			imgURL = "img/bubble-sm-blue.png";
        		if(imgRand == 2)
        			imgURL = "img/bubble-sm-green.png";
    			if(imgRand == 3)
    				imgURL = "img/bubble-sm-purple.png";
    			if(imgRand == 4)
    				imgURL = "img/bubble-sm-yellow.png";			        		
        	}
        	if(sizeRand == 5){
        		sizeWH = 25;
        		imgRand = ran(1,4);
        		if(imgRand == 1)
        			imgURL = "img/bubble-xs-blue.png";
        		if(imgRand == 2)
        			imgURL = "img/bubble-xs-green.png";
    			if(imgRand == 3)
    				imgURL = "img/bubble-xs-purple.png";
    			if(imgRand == 4)
    				imgURL = "img/bubble-xs-yellow.png";				        		
        	}

            circs.push(paper.image(imgURL, ran(0,650), ran(0,700), sizeWH, sizeWH));
            console.log(imgURL+": H: "+sizeWH+" W: "+sizeWH);
        }

        moveIt();
    };
}());

function movePinUp(){
    $("#animated-pin").animate({top: "+=18"}, 1500, movePinDown);
}
function movePinDown(){
    $("#animated-pin").animate({top: "-=18"}, 1500, movePinUp);
}

function moveSMelipseRight(){
	$("#small-elipse").animate({left: "+=2"}, 1800, moveSMelipseLeft);	
}
function moveSMelipseLeft(){
	$("#small-elipse").animate({left: "-=2"}, 1800, moveSMelipseRight);	
}

function rotateBGelipse(){
    var startAngle = 0;
    var unit = 10;

    var animate = function () {
        var rad = startAngle * (Math.PI / 180);
        $('#big-elipse').css({
            left: -300 + Math.cos(rad) * unit + 'px',
            top: unit * (1 - Math.sin(rad)) + 'px'
        });
        startAngle--;
    }
    var timer = setInterval(animate, 10);
}

function init(){
	var width = $(window).width();
	console.log(width);
	var panelWidth = 0.5;

	if(width<=414)
		panelWidth = 0.95;
	else if(width<=320)
		panelWidth = 1;

	

	$('#slider-one').movingBoxes({
	startPanel   : 2,      // start with this panel
	reducedSize  : 0.5,    // non-current panel size: 80% of panel size
	wrap         : false,   // if true, the panel will "wrap" (it really rewinds/fast forwards) at the ends
	buildNav     : false,   // if true, navigation links will be added
	navFormatter : function(){ return "&#9679;"; } // function which returns the navigation text for each panel
	,panelWidth   : panelWidth,
	initAnimation: true,
	easing       : 'swing',
	completed: function(e, slider, tar){
		
		$("#movingbox-label").text(slider.curPanel+"/3");
	}
	});

	var mb = $('#slider-one').data('movingBoxes');

	$("#movingbox-next").bind("click", function(){
		mb.goForward();
	});

	$("#movingbox-prev").bind("click", function(){
		mb.goBack();
	});
}