$(document).ready(function() {	
	init();

	$("#mobileShowlogin").click(function(){
		$("#mobileLogin").toggle();
	});

	moveMouseScrollUp();
	$("#button-scroll-click").click(function() {
		console.log("scroll up");
		$("html, body").animate({
	        scrollTop: $("#gftnow-landing-how").offset().top
	    }, 1000);
	});

	function fadeInOut(item) {
		item.fadeIn(1000).delay(3000).fadeOut(1000, function() {
			if (item.next().length) // if there is a next element
			{
				fadeInOut(item.next());
			} // use it
			else {
				fadeInOut(item.siblings(':first'));
			} // if not, then use go back to the first sibling
		});
	}
	fadeInOut(jQuery('#text-slider li:first-child'));
	fadeInOut(jQuery('#text-slider2 li:first-child'));

	$("#signin-link").click(function(){
		$(".gftnow-signbox").fadeToggle();
	});
	$("#forgot-pw-link").click(function(){
		$("#forgotPasswordForm").toggle();
	});

	$("#forgotPasswordForm").validate({
		errorPlacement: function(){
			return false;
		},
		rules:{
			email: "required"
		},
		submitHandler: function(form){
	    	$("#mask1").show();
			console.log("submit");
			console.log($(form.email).val());

			var email = $(form.email).val();
			var haserror = 0;

			firebase.auth().sendPasswordResetEmail(email).catch(function(error){
				if(errorCode=="auth/invalid-email"){
					$("#errorCode1 .alert").text(errorMessage);
				  	haserror = 1;
				  	$("#errorCode1").show();
				}
				if(errorCode=="auth/user-not-found"){
					$("#errorCode1 .alert").text(errorMessage);
				  	haserror = 1;
				  	$("#errorCode1").show();
				}
			}).then(function(){
				if(haserror==0){
					$("#mask2").hide();
					$("#gftnow-alert-modal-content").html("Password reset email sent.");
					$("#gftnow-alert-modal").css("position", "fixed").modal("show");
				}
			});
		}
	});

	$("#loginform").validate({
		errorPlacement: function(){
			return false;
		},
		rules:{
			email: "required",
			password: "required"
		},
		submitHandler: function(form){
			console.log("submit");
			
			console.log($(form.email).val());
			console.log($(form.password).val());

			var email = $(form.email).val();
			var password = $(form.password).val();
			var haserror = 0;

			firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
			  // Handle Errors here.
			  var errorCode = error.code;
			  var errorMessage = error.message;

			  if(errorCode=="auth/wrong-password")
			  {
				  	$("#errorCode .alert").text(errorMessage);
				  	haserror = 1;
				  	$("#errorCode").show();
			  }
			  
			  // ...
			}).then(function(user){

				if(haserror==0){
					console.log(user);

					var user = firebase.auth().currentUser;

					user.getToken().then(function(data) {
	      
		    			var token = data;
								
						jQuery.ajax({
							url: "business/login",
							type: "post",
							dataType : "json",
							data: { token:token },
							success: function(data){
								if(data.userInfo){
									if(data.userInfo.account.state=="Enabled"){
										window.location = "profile";
									}else{
										$("#errorCode .alert").text("Cannot login. This user is blocked or do not exist.");
										$("#errorCode").show();
									}
								}else{
									console.log("cant login");
									$("#errorCode .alert").text("Cannot login. This user is blocked or do not exist.");
									$("#errorCode").show();
									$("#mask2").hide();
								}
							},error: function(err){
								console.log(err.responseText);
							}
						});
	    			});
				}
			});
		}
	});

	$("#loginform2").validate({
		errorPlacement: function(){
			return false;
		},
		rules:{
			email: "required",
			password: "required"
		},
		submitHandler: function(form){
			console.log("submit");
			
			console.log($(form.email).val());
			console.log($(form.password).val());

			var email = $(form.email).val();
			var password = $(form.password).val();
			var haserror = 0;

			firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
			  // Handle Errors here.
			  var errorCode = error.code;
			  var errorMessage = error.message;

			  if(errorCode=="auth/wrong-password")
			  {
				  

				  	$("#errorCode2 .alert").text(errorMessage);
				  	haserror = 1;
				  	$("#errorCode2").show();
			  }
			  
			  // ...
			}).then(function(user){

				if(haserror==0){
					console.log(user);

					var user = firebase.auth().currentUser;

					user.getToken().then(function(data) {
	      
		    			var token = data;

		    			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
						jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });
								
						jQuery.ajax({
							url: "business/login",
							type: "post",
							dataType : "json",
							data: { token:token },
							success: function(data){
								jQuery(document).ajaxComplete(function(){ $("#mask2").hide(); });
								$("#mask2").hide();
								if(data.result=="ok"){
									if(data.state=="Enabled"){
										window.location = "profile";
									}else{
										$("#errorCode2 .alert").text("Cannot login. This user is blocked or do not exist.");
										$("#errorCode2").show();
									}
								}else{
									console.log("cant login");
									$("#errorCode2 .alert").text("Cannot login. This user is blocked or do not exist.");
									$("#errorCode2").show();
								}
							},error: function(err){
								console.log(err.responseText);
							}
						});
	    			});
				}
			});
		}
	});

	});

	function moveMouseScrollUp(){
		$("#moving-mousescroll").animate({top: "+=20"}, 500, moveMouseScrollDown);
	}
	function moveMouseScrollDown(){
		$("#moving-mousescroll").animate({top: "-=20"}, 500, moveMouseScrollUp);
	}

	function init(){
	var width = $(window).width();
	console.log(width);
	var panelWidth = 0.5;

	if(width<=414)
		panelWidth = 0.95;
	else if(width<=320)
		panelWidth = 1;



	$('#slider-one').movingBoxes({
	startPanel   : 2,      // start with this panel
	reducedSize  : 0.5,    // non-current panel size: 80% of panel size
	wrap         : false,   // if true, the panel will "wrap" (it really rewinds/fast forwards) at the ends
	buildNav     : false,   // if true, navigation links will be added
	navFormatter : function(){ return "&#9679;"; } // function which returns the navigation text for each panel
	,panelWidth   : panelWidth,
	initAnimation: true,
	easing       : 'swing',
	completed: function(e, slider, tar){
		
		$("#movingbox-label").text(slider.curPanel+"/3");
	}
	});

	var mb = $('#slider-one').data('movingBoxes');

	$("#movingbox-next").bind("click", function(){
		mb.goForward();
	});

	$("#movingbox-prev").bind("click", function(){
		mb.goBack();
	});
}