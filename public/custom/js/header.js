var config = {
	apiKey: "AIzaSyD57sJlGpTwEjkX7tjsUqbpvYgQOuLDmOk",
	authDomain: "gftnow.firebaseapp.com",
	databaseURL: "https://gftnow.firebaseio.com",
	projectId: "gftnow",
	storageBucket: "gftnow.appspot.com",
	messagingSenderId: "656938776598"
};
firebase.initializeApp(config);

$(document).ready(function(){
	$("#logout").show();

	$("#logout").click(function(){
		$("#mask2").show();
		window.location.href = "/logout";
	});

	$('.minifyme, .hide-menu').click(function(e) {
		$('body').toggleClass("minified");
		$(this).effect("highlight", {}, 500);
		e.preventDefault();
	});

	jQuery.ajaxSetup({async:true});

	jQuery(document).ajaxStart(function(){
		$("#mask2").show();
	});
	jQuery(document).ajaxComplete(function(){	
		$("#mask2").hide();
	});
});
