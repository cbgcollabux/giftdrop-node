var map;
$(document).ready(function() {
	var stripe = Stripe('pk_test_j8S6BcBIvH7xPmz2W8XgXfWA');
	var elements = stripe.elements();

	var style = {
	  base: {
	    color: '#32325d',
	    lineHeight: '24px',
	    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
	    fontSmoothing: 'antialiased',
	    fontSize: '16px',
	    '::placeholder': {
	      color: '#aab7c4'
	    }
	  },
	  invalid: {
	    color: '#fa755a',
	    iconColor: '#fa755a'
	  }
	};

	var card = elements.create('card', {style: style});
	card.mount('#card-element');

	card.addEventListener('change', function(event) {
	  var displayError = document.getElementById('card-errors');
	  if (event.error) {
	    displayError.textContent = event.error.message;
	  } else {
	    displayError.textContent = '';
	  }
	});

	var form = document.getElementById('payment-form');
	form.addEventListener('submit', function(event) {
	  $("#mask2").show();
	  event.preventDefault();
	  stripe.createToken(card).then(function(result) {
	    if (result.error) {
	      $("#mask2").hide();
	      // Inform the user if there was an error
	      var errorElement = document.getElementById('card-errors');
	      errorElement.textContent = result.error.message;
	    } else {
	    	$("#mask2").show();
	 	 	$("#submit").hide();
	      stripeTokenHandler(result.token);
	    }
	  });
	});



	function stripeTokenHandler(token){

		jQuery(document).ajaxStart(function(){
				$("#mask2").show();
			});

          console.log(token);
          if(token)
          {
          	jQuery(document).ajaxStart(function(){
				$("#mask2").show();
			});

			jQuery(document).ajaxComplete(function(){	
				$("#mask2").show();
			});

			console.log($("#amount").val());

          	jQuery.ajax({
	          	url: "campaign_paypal/stripe",
	          	type: "post",
	          	dataType: "json",
	          	data: { token: token.id, amount: $("#amount").val() },
	          	success: function(data){
	          		console.log(JSON.stringify(data));

	          		if(data.result=="success"){
	          			$("#paymentsubmit").submit();
	          		}

	          	},error: function(err){
	          		jQuery(document).ajaxComplete(function(){	
						$("#mask2").hide();
					});
	          		$("#submit").show();
	          		$("#mask2").hide();
          			$("#gftnow-alert-modal-content").html("Payment not successful. Pls try again.");
					$("#gftnow-alert-modal").css("position", "fixed").modal("show");
					$("#submit-button").show();
	          	}
	          });
          }else{
          	jQuery(document).ajaxStart(function(){
				$("#mask2").hider();
			});
          }

	}
});