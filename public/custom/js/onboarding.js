$(document).ready(function() {
	checkvalidation();

	var image;

	fileUpload_();

	function fileUpload_(){
		$("#fileupload").change(function(){
			var fileExt = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];

			if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExt) == -1) {
				$("#gftnow-alert-modal").find(".modal-body").html("<center>Only image files are accepted</center>");
				$("#gftnow-alert-modal").modal("show");
        	} else {
        		$("#uploadForm").ajaxSubmit({
					url: 'upload',
					type: 'post',
					dataType: 'json',
					success: function(data) {
						console.log(data);
			            $("#mask2").hide();

			            $("#uploadModal").find(".modal-body").html("<div id=\'img-container\'><img id=\'image2\' src=\'"+data.url+"\' alt=\'Picture\'></div>");
		        		
		            	image = $('#image2');

		            	var cropper = image.cropper({
						    aspectRatio: 1/1,
						    minContainerWidth: 555,
						    minContainerHeight: 300,
						    cropBoxResizable: false,
						    built: function () {
						      image.cropper("setCropBoxData", { width: 250, height: 250 });
						    },
						    crop: function(e) {
						      console.log(e.x);
						      console.log(e.y);
						      console.log(e.width);
						      console.log(e.height);
						      console.log(e.rotate);
						      console.log(e.scaleX);
						      console.log(e.scaleY);
						    }
						});
		              
						$("#mask2").hide();
						$("#uploadModal").modal({backdrop: 'static', keyboard: false}, "toggle");

						if(image){
							$("#cropnow").removeClass("gftnow-btn-default").addClass("gftnow-btn-success");
						}

						cropnow_();
			        },
					error: function(err){
						console.log(err.responseText);
						$("#mask2").hide();
					}
				});
        	}
		});
	}

	function cropnow_(){
		$("#cropnow").bind("click",function(){
			if($(this).hasClass("gftnow-btn-success")){
				$("#mask2").show();
				image.cropper('getCroppedCanvas',{
					width:300,
					height:300,
					fillColor: '#fff',
					imageSmoothingEnabled: true,
					imageSmoothingQuality: 'high'
				}).toBlob(function (blob) {
		      		$("#cropnow").unbind("click");
		      		var reader = new FileReader();
		      		reader.readAsDataURL(blob); 
					reader.onloadend = function() {
						function base64ToFile(base64Data, tempfilename, contentType) {
							contentType = contentType || '';
							var sliceSize = 1024;
							var byteCharacters = atob(base64Data);
							var bytesLength = byteCharacters.length;
							var slicesCount = Math.ceil(bytesLength / sliceSize);
							var byteArrays = new Array(slicesCount);

							for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
								var begin = sliceIndex * sliceSize;
								var end = Math.min(begin + sliceSize, bytesLength);

								var bytes = new Array(end - begin);
								for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
									bytes[i] = byteCharacters[offset].charCodeAt(0);
								}
									byteArrays[sliceIndex] = new Uint8Array(bytes);
								}
							var file = new File(byteArrays, tempfilename, { type: contentType });
							return file;
						}

					    var base64data = reader.result.substr(reader.result.indexOf(',') + 1);
					    var file = base64ToFile(base64data, 'tempName', 'image/png');

					    var fd = new FormData();
					    fd.append('filename', file);

					    jQuery.ajax({
					    	url: "crop",
							type: "post",
							dataType: "json",
							data: fd,
							processData: false,
							contentType: false,
							success: function(data) {
								console.log(data);

								$("#clogo").val(data.url);
								$(".uploadLogo").attr("style","background-image:url("+data.url+");background-size:cover;background-position: center center;background-repeat:no-repeat;");
								$(".uploadLogo").html('<form id="uploadForm" method="post" enctype="multipart/form-data" action="upload"><input id="fileupload" name="filename" type="file"></form>');

								$("#mask2").hide();
								$("#uploadModal").find(".modal-body").html("");
								$("#uploadModal").modal("toggle");

								fileUpload_();

							},
							error: function(err){
								console.log(err.responseText);
								$("#mask2").hide();
								$("#uploadModal").modal("toggle");
							}
					    });
					}
		  		});
			}
		});
	}


	jQuery.validator.addMethod("zipcode", function(value, element) {
	  return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
	}, "Please provide a valid zipcode.");

	$.validator.addMethod("pwcheck", function(value){
		return /^[a-zA-Z0-9!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]+$/.test(value)
		&& /[a-z]/.test(value) // has a lowercase letter
		&& /[A-Z]/.test(value) // has a uppercase letter
		&& /\d/.test(value) //has a digit
		&& /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/.test(value) // has a special character
	},"Must be at least 8 characters and consist of lowercase letter, uppercase letter, number and special characters");

	$("#form1").validate({
		rules:{
			company: "required",
			email: {
				required: true,
				email: true
			},
			password:{
				required: true,
				pwcheck: true,
				minlength: 8
			},
			repassword: {
				required: true
			}
		},
		submitHandler: function(form){

			$("#mask2").show();
			
			if($("#clogo").val()==""){
				$("#gftnow-alert-modal-content").text("Please upload your logo");
				$("#gftnow-alert-modal").modal("show");
				$("#mask2").hide();
			}else if($("#password").val()!=$("#repassword").val()){
				$("#gftnow-alert-modal-content").text("Password do not match");
				$("#gftnow-alert-modal").modal("show");
				$("#mask2").hide();
			}else{

				firebase.auth().createUserWithEmailAndPassword( $("#email").val(), $("#password").val() ).catch(function(error) {
		 
				  var errorCode = error.code;
				  var errorMessage = error.message;

				  console.log(errorCode);
				  console.log(errorMessage);
				 	
				  if(errorCode=="auth/email-already-in-use"){
				  	$("#gftnow-alert-modal-content").text(errorMessage);
					$("#gftnow-alert-modal").modal("show");

					$("#mask2").hide();
				}
				  
				}).then(function(user){
					if(user){
						
						var cuser = firebase.auth().currentUser;

						cuser.getToken().then(function(data){
							var token = data;

							user.updateProfile({
			  	  				displayName: $("#company").val(),
							  	email: $("#email").val(),
							  	photoURL: $("#clogo").val(),
								}).then(function() {

									/*var company = {};
									company.name = $("#company").val();
									company.email = $("#email").val();
									company.password = $("#password").val();
									company.logoURL = $("#clogo").val();*/

									var name = $("#company").val();
									var email = $("#email").val();
									var password = $("#password").val();
									var logoURL = $("#clogo").val();

									console.log(token);

									jQuery.ajax({
								    	url: "onboarding/submit",
								    	type: "post",
								    	dataType: "json",
								    	//data: {token:token, company:company},
								    	data: { token:token, name:name, email:email, password:password, logoURL:logoURL },
								    	success: function(data){
								    		console.log(data);
								    		window.location = "onboarding-1";
								    	},error: function(err){
								    		console.log(err.responseText);
								    	}
								    });
							  		
								}, function(error) {
									console.log(error);
							});
						});
					}
				});
			}
				
		}
	});

	$("input").focus(function(){
		checkvalidation();
	});

	$("input").keyup(function(){
		checkvalidation();
	});
});

function checkvalidation(){
	var err = 0;

	$("input").each(function(){
		if ( $(this).hasClass("invalid") )
			err++;
	});

	if( $("#company").val() == "" )
		err++;

	if( $("#email").val() == "" )
		err++;

	if( $("#password").val() == "" )
		err++;

	if( $("#password").val() != $("#repassword").val() )
		err++;

	if( $("#clogo").val() == "" )
		err++;


	if(err==0){
		$("#submit").removeClass("gftnow-btn-default");
		$("#submit").addClass("gftnow-btn-success");
	}else{
		$("#submit").addClass("gftnow-btn-default");
		$("#submit").removeClass("gftnow-btn-success");
	}
}