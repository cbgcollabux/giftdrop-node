$(document).ready(function(){
	
	$(this).on('click', function(e) {
	    $(".balloon-container").hide();
	}).on('click', '#notif-bell', function(e) {
	    e.stopPropagation();
	}).on('click', '.balloon-container', function(e){
		e.stopPropagation();
	});

	$("input, textarea").click(function(){

		if($(this).attr("placeholder") !=""){
			var ph = $(this).attr("placeholder");
			var id = $(this).attr("id");

			$(this).attr("data-placeholder", ph);
			$(this).attr("placeholder","");
			$("#"+id+"-placeholder").text(ph);

			$("#"+id+"-placeholder").addClass("active");
		}
		
	});

	$("input, textarea").focusout(function(){
		if($(this).val() == ''){
			if($(this).attr("placeholder") ==""){
				var id = $(this).attr("id");
				ph = $(this).attr("data-placeholder");
				$("#"+id+"-placeholder").removeClass("active");

				setTimeout(function(){
					console.log("ph "+ph);
					$("#"+id+"-placeholder").text("");
					$("#"+id).attr("placeholder",ph);
				 }, 500);
			}
		}
	});
});

function showballon(){
	$(".balloon-container").show();
}

function showballon1(){
	$(".balloon-container1").show();
}

CustomMarker.prototype = new google.maps.OverlayView();

function CustomMarker(latlng, map, args,quantity) {
	this.latlng = latlng;	
	this.args = args;	
	this.setMap(map);	
	this.quantity = quantity;
}

CustomMarker.prototype.draw = function(){
	var self = this;
	var div = this.div;
	var quantity = this.quantity;

	if (!div) {
		div = this.div = document.createElement('div');
		div.className = 'gftnow-numbered-marker';

		var textnode = document.createTextNode(quantity);  
		div.appendChild(textnode);
		
		if (typeof(self.args.marker_id) !== 'undefined') {
			div.dataset.marker_id = self.args.marker_id;
		}

		google.maps.event.addDomListener(div, "click", function(event) {			
			google.maps.event.trigger(self, "click");
		});

		var panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	}

	var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

	if (point) {
			div.style.left = point.x + 'px';
			div.style.top = point.y + 'px';
	}
}

CustomMarker.prototype.getPosition = function(){
	return this.latlng;	
};

//Image Marker
ImageMarker.prototype = new google.maps.OverlayView();

function ImageMarker(latlng, map, args, image) {
	this.latlng = latlng;	
	this.args = args;	
	this.setMap(map);	
	this.image = image;
}

ImageMarker.prototype.draw = function(){
	var self = this;
	var div = this.div;
	var image = this.image;

	if (!div) {
		div = this.div = document.createElement('div');
		div.className = 'gftnow-image-marker';

		var usr_img = document.createElement("img");
		usr_img.src = image;
		div.appendChild(usr_img);
		
		$(div).find("img").css({
			"height": "60px",
			"width": "60px",
			"margin-top": "-23px"
		});

		if (typeof(self.args.marker_id) !== 'undefined') {
			div.dataset.marker_id = self.args.marker_id;
		}

		google.maps.event.addDomListener(div, "click", function(event) {			
			google.maps.event.trigger(self, "click");
		});

		var panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	}

	var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

	if (point) {
			div.style.left = point.x + 'px';
			div.style.top = point.y + 'px';
	}
}

ImageMarker.prototype.getPosition = function(){
	return this.latlng;	
};